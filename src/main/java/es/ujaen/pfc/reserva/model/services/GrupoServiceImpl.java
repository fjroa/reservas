/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: GrupoExternoServiceImpl.java
 * Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.Search;
import com.googlecode.genericdao.search.SearchResult;

import es.ujaen.pfc.reserva.model.dao.GrupoDAO;
import es.ujaen.pfc.reserva.model.objects.Actividad;
import es.ujaen.pfc.reserva.model.objects.EstadoGrupo;
import es.ujaen.pfc.reserva.model.objects.Grupo;
import es.ujaen.pfc.reserva.model.objects.Profesor;

@Service
public class GrupoServiceImpl implements GrupoService {

	GrupoDAO dao;
	
	@Autowired
	public void setDao(GrupoDAO dao) {
		this.dao = dao;
	}
	
	public void save(Grupo grupo) {
		dao.save(grupo);
	}

	public void delete(Grupo grupo) {
		dao.removeById(grupo.getId());
	}

	public List<Grupo> findAll() {
		return dao.findAll();
	}

	public List<Grupo> search(ISearch search) {
		return dao.search(search);
	}

	public SearchResult<Grupo> searchAndCount(ISearch search) {
		return dao.searchAndCount(search);
	}

	public Grupo findById(long id) {
		return dao.find(id);
	}

	public void flush() {
		dao.flush();
	}

	public List<Grupo> findByActividad(Actividad actividad) {
		return dao.search(new Search().addFilterEqual("actividad", actividad));
	}

	public List<Grupo> findByProfesor(Profesor profesor) {
		return dao.search(new Search().addFilterEqual("profesor", profesor));
	}

	public List<Grupo> findByEstado(EstadoGrupo estado) {
		return dao.search(new Search().addFilterEqual("estado", estado));
	}
}