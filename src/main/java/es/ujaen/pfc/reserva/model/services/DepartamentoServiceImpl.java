/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: DepartamentoExternoServiceImpl.java
 * Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.Search;
import com.googlecode.genericdao.search.SearchResult;

import es.ujaen.pfc.reserva.model.dao.DepartamentoDAO;
import es.ujaen.pfc.reserva.model.objects.Actividad;
import es.ujaen.pfc.reserva.model.objects.Departamento;

@Service
public class DepartamentoServiceImpl implements DepartamentoService {

	DepartamentoDAO dao;

	@Autowired
	public void setDao(DepartamentoDAO dao) {
		this.dao = dao;
	}

	public void save(Departamento Departamento) {
		dao.save(Departamento);
	}

	public void delete(Departamento Departamento) {
		dao.removeById(Departamento.getIdUsuario());
	}

	public List<Departamento> findAll() {
		return dao.findAll();
	}

	public List<Departamento> search(ISearch search) {
		return dao.search(search);
	}

	public SearchResult<Departamento> searchAndCount(ISearch search) {
		return dao.searchAndCount(search);
	}

	public Departamento findById(String idUsuario) {
		return dao.find(idUsuario);
	}

	public Departamento findByNombre(String nombre) {
		if (nombre == null)
			return null;
		return dao.searchUnique(new Search().addFilterEqual("nombre", nombre));
	}

	public void flush() {
		dao.flush();
	}

	public List<Departamento> findByActividad(Actividad actividad) {
		return dao.search(new Search().addFilterSome("actividades",
				Filter.equal("codigo", actividad.getCodigo())));
	}

}
