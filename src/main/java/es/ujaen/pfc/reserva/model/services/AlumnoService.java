/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: AlumnoExternoService.java
 * Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.services;

import java.util.List;

import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.SearchResult;

import es.ujaen.pfc.reserva.model.objects.Actividad;
import es.ujaen.pfc.reserva.model.objects.Alumno;

public interface AlumnoService {

	public void save(Alumno alumno);

	public void delete(Alumno alumno);

	public List<Alumno> findAll();

	public List<Alumno> search(ISearch search);

	public SearchResult<Alumno> searchAndCount(ISearch search);

	public Alumno findById(String idUsuario);

	public Alumno findByNombre(String nombre);

	public void flush();

	public List<Alumno> findByActividad(Actividad actividad);

}
