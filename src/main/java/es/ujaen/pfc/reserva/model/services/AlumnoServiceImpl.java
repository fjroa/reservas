/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: AlumnoExternoServiceImpl.java
 * Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.Search;
import com.googlecode.genericdao.search.SearchResult;

import es.ujaen.pfc.reserva.model.dao.AlumnoDAO;
import es.ujaen.pfc.reserva.model.objects.Actividad;
import es.ujaen.pfc.reserva.model.objects.Alumno;

@Service
public class AlumnoServiceImpl implements AlumnoService {

	AlumnoDAO dao;

	@Autowired
	public void setDao(AlumnoDAO dao) {
		this.dao = dao;
	}

	public void save(Alumno Alumno) {
		dao.save(Alumno);
	}

	public void delete(Alumno Alumno) {
		dao.removeById(Alumno.getIdUsuario());
	}

	public List<Alumno> findAll() {
		return dao.findAll();
	}

	public List<Alumno> search(ISearch search) {
		return dao.search(search);
	}

	public SearchResult<Alumno> searchAndCount(ISearch search) {
		return dao.searchAndCount(search);
	}

	public Alumno findById(String idUsuario) {
		return dao.find(idUsuario);
	}

	public Alumno findByNombre(String nombre) {
		if (nombre == null)
			return null;
		return dao.searchUnique(new Search().addFilterEqual("nombre", nombre));
	}

	public void flush() {
		dao.flush();
	}

	public List<Alumno> findByActividad(Actividad actividad) {
		return dao.search(new Search().addFilterSome("actividades",
				Filter.equal("codigo", actividad.getCodigo())));
	}

}
