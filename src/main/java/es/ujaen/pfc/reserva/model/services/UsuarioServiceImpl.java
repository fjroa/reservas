/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: UsuarioExternoServiceImpl.java
* Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.Search;
import com.googlecode.genericdao.search.SearchResult;

import es.ujaen.pfc.reserva.model.dao.UsuarioDAO;
import es.ujaen.pfc.reserva.model.objects.Usuario;

@Service
public class UsuarioServiceImpl implements UsuarioService {

UsuarioDAO dao;
	
	@Autowired
	public void setDao(UsuarioDAO dao) {
		this.dao = dao;
	}
	
	public void save(Usuario Usuario){
		dao.save(Usuario);
	}

	public void delete(Usuario Usuario){
		dao.removeById(Usuario.getIdUsuario());
	}

	public List<Usuario> findAll() {
		return dao.findAll();
	}

	public List<Usuario> search(ISearch search) {
		return dao.search(search);
	}

	public SearchResult<Usuario> searchAndCount(ISearch search) {
		return dao.searchAndCount(search);
	}

	public Usuario findById(String idUsuario) {
		return dao.find(idUsuario);
	}

	public Usuario findByNombre(String nombre) {
		if (nombre == null)
			return null;
		return dao.searchUnique(new Search().addFilterEqual("nombre", nombre));
	}

	public void flush() {
		dao.flush();
	}


}
