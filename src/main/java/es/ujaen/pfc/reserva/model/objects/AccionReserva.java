package es.ujaen.pfc.reserva.model.objects;

public enum AccionReserva {
	CREACION, PREASIGNACION, ASIGNACION, DENEGACION, PERMUTA, ELIMINACION;
}
