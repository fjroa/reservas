/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: UsuarioExterno.java
 * Creado: 21-nov-2011
 * Version: 1.0
 * Descripcion: Entidad para la consultad de los datos 
 * las asignaturas de la universidad.
 */
package es.ujaen.pfc.reserva.model.objects;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Actividad implements java.io.Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long codigo;
	private String nombre;
	private String abreviatura;
	@ManyToOne
	private Usuario coordinador;
	@OneToMany(mappedBy = "actividad")
	private List<Grupo> grupos;
	
	public Actividad() {
		super();
		grupos = new ArrayList<Grupo>();
	}
	
	public long getCodigo() {
		return codigo;
	}
	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}
	public Usuario getCoordinador() {
		return coordinador;
	}
	public void setCoordinador(Usuario coordinador) {
		this.coordinador = coordinador;
	}

	public List<Grupo> getGrupos() {
		return grupos;
	}

	public void setGrupos(List<Grupo> grupos) {
		this.grupos = grupos;
	}	
	public void addGrupo(Grupo g) {
		grupos.add(g);
	}
}
