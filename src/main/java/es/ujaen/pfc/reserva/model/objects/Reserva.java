/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: UsuarioExterno.java
 * Creado: 21-nov-2011
 * Version: 1.0
 * Descripcion: Entidad para la consultad de los datos 
 * las asignaturas de la universidad.
 */
package es.ujaen.pfc.reserva.model.objects;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Reserva implements java.io.Serializable {

	public Reserva() {
		super();
		this.historicosReserva = new ArrayList<HistoricoReserva>();
	}

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne
	private Alumno alumno;

	private float promedio;

	@ManyToOne
	private Grupo grupo;
	
	@Enumerated
	private EstadoReserva estadoReserva;

	@OneToMany(mappedBy = "reserva", cascade = CascadeType.ALL)
	private List<HistoricoReserva> historicosReserva;
	
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public EstadoReserva getEstadoReserva() {
		return estadoReserva;
	}

	public void setEstadoReserva(EstadoReserva estadoReserva) {
		this.estadoReserva = estadoReserva;
	}

	public float getPromedio() {
		return promedio;
	}

	public void setPromedio(float promedio) {
		this.promedio = promedio;
	}

	public List<HistoricoReserva> getHistoricosReserva() {
		return historicosReserva;
	}

	public void setHistoricosReserva(List<HistoricoReserva> historicosReserva) {
		this.historicosReserva = historicosReserva;
	}

	public void addHistoricoReserva(HistoricoReserva historicoReserva) {
		this.historicosReserva.add(historicoReserva);
	}
}
