package es.ujaen.pfc.reserva.model.objects;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("Departamento")
public class Departamento extends Usuario {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7945509942758493663L;

	@ManyToOne
	private Centro centro;
	
	public Departamento() {
		super();
	}

	public Centro getCentro() {
		return centro;
	}

	public void setCentro(Centro centro) {
		this.centro = centro;
	}

	
}
