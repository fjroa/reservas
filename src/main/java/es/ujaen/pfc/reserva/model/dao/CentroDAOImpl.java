/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: DepartamentoExternoDAOImpl.java
* Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.dao;

import org.springframework.stereotype.Repository;

import es.ujaen.pfc.reserva.model.objects.Centro;

@Repository
public class CentroDAOImpl extends BaseDAO<Centro, String> implements CentroDAO {

}