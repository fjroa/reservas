/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: AlumnoExternoDAO.java
 * Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

import es.ujaen.pfc.reserva.model.objects.Alumno;

// TODO: Auto-generated Javadoc
/**
 * The Interface AlumnoExternoDAO.
 */
public interface AlumnoDAO extends GenericDAO<Alumno, String>{


}