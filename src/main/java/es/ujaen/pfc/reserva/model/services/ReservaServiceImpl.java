/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: ReservaExternoServiceImpl.java
 * Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.Search;
import com.googlecode.genericdao.search.SearchResult;

import es.ujaen.pfc.reserva.model.dao.GrupoDAO;
import es.ujaen.pfc.reserva.model.dao.ReservaDAO;
import es.ujaen.pfc.reserva.model.objects.Alumno;
import es.ujaen.pfc.reserva.model.objects.EstadoReserva;
import es.ujaen.pfc.reserva.model.objects.Grupo;
import es.ujaen.pfc.reserva.model.objects.Reserva;

@Service
public class ReservaServiceImpl implements ReservaService {

	ReservaDAO dao;

	@Autowired
	public void setDao(ReservaDAO dao) {
		this.dao = dao;
	}

	GrupoDAO grupoDAO;

	@Autowired
	public void setGrupoDao(GrupoDAO grupoDAO) {
		this.grupoDAO = grupoDAO;
	}

	public void save(Reserva reserva) {
		actualizarGrupo(reserva, reserva.getEstadoReserva(), true);
		dao.save(reserva);
	}

	public void delete(Reserva reserva) {

		if (actualizarGrupo(reserva, reserva.getEstadoReserva(), false)) {
			dao.removeById(reserva.getId());
		}

	}

	public List<Reserva> findAll() {
		return dao.findAll();
	}

	public List<Reserva> search(ISearch search) {
		return dao.search(search);
	}

	public SearchResult<Reserva> searchAndCount(ISearch search) {
		return dao.searchAndCount(search);
	}

	public Reserva findById(long id) {
		return dao.find(id);
	}

	public void flush() {
		dao.flush();
	}

	public List<Reserva> findByAlumno(Alumno alumno) {
		Search s = new Search();
		s.addFilterEqual("alumno", alumno);
		s.addSortAsc("grupo.actividad.codigo");
		return dao.search(s);

	}

	public List<Reserva> findByGrupo(Grupo grupo) {
		return dao.search(new Search().addFilterEqual("grupo", grupo));
	}

	public List<Reserva> findAsignadasByGrupo(Grupo grupo) {
		Search s = new Search();
		s.addFilterEqual("grupo", grupo);
		s.addFilterNotEqual("estadoReserva", EstadoReserva.SOLICITUD);
		s.addSortDesc("promedio");
		return dao.search(s);
	}

	public List<Reserva> findSolicitudesByGrupo(Grupo grupo) {
		Search s = new Search();
		s.addFilterEqual("grupo", grupo);
		s.addFilterEqual("estadoReserva", EstadoReserva.SOLICITUD);
		s.addSortDesc("promedio");
		return dao.search(s);
	}

	public List<Reserva> findByFechas(Date fechaInicio, Date fechaFin) {
		Search s = new Search();
		s.addFilterGreaterOrEqual("fechaInicio", fechaInicio);
		s.addFilterGreaterOrEqual("fechaFin", fechaFin);
		return dao.search(s);
	}

	public List<Reserva> findByEstadoReserva(EstadoReserva estado) {
		return dao.search(new Search().addFilterEqual("estadoReserva", estado));
	}

	public Reserva findByAlumnoGrupo(Alumno alumno, Grupo grupo) {
		Search s = new Search();
		s.addFilterEqual("alumno", alumno);
		s.addFilterEqual("grupo", grupo);
		return dao.searchUnique(s);
	}

	public void saveReservaPreasignada(Reserva reserva) {
		actualizarGrupo(reserva, EstadoReserva.PREASIGNADA, true);
		actualizarGrupo(reserva, EstadoReserva.SOLICITUD, false);
		dao.save(reserva);

	}

	private boolean actualizarGrupo(Reserva reserva, EstadoReserva estado, boolean create) {
		Grupo grupo = reserva.getGrupo();

		if (create) {
			if (estado.equals(EstadoReserva.SOLICITUD)) {
				grupo.setNumeroReservasSolicitadas(grupo
						.getNumeroReservasSolicitadas() + 1);
				float promedio = grupo.getNotaCorteReservas();
				if (promedio < reserva.getPromedio()) {
					grupo.setNotaCorteReservas(reserva.getPromedio());
				}
			} else {
				grupo.setNumeroPlazasOcupadas(grupo.getNumeroPlazasOcupadas() + 1);
				float promedio = grupo.getNotaCorte();
				if (promedio < reserva.getPromedio()) {
					grupo.setNotaCorte(reserva.getPromedio());
				}
			}
			grupoDAO.save(grupo);
		} else {
			if (estado.equals(EstadoReserva.SOLICITUD)) {
				if (grupo.getNumeroReservasSolicitadas() > 0) {
					grupo.setNumeroReservasSolicitadas(grupo
							.getNumeroReservasSolicitadas() - 1);
					if (grupo.getNotaCorteReservas() == reserva.getPromedio()) {
						List<Reserva> reservasGrupo = this
								.findSolicitudesByGrupo(grupo);
						float menorNotaCorte = Float.MAX_VALUE;
						for (Reserva r : reservasGrupo) {
							if (r.getId() != reserva.getId()) {
								if (r.getPromedio() < menorNotaCorte) {
									menorNotaCorte = r.getPromedio();
								}
							}
						}
						if (menorNotaCorte == Float.MAX_VALUE) {
							menorNotaCorte = 0f;
						}
						grupo.setNotaCorteReservas(menorNotaCorte);
					}
					grupoDAO.save(grupo);
				} else {
					return false;
				}
			} else {
				if (grupo.getNumeroPlazasOcupadas() > 0) {
					grupo.setNumeroPlazasOcupadas(grupo
							.getNumeroPlazasOcupadas() - 1);
					if (grupo.getNotaCorte() == reserva.getPromedio()) {
						List<Reserva> reservasGrupo = this
								.findAsignadasByGrupo(grupo);
						float menorNotaCorte = Float.MAX_VALUE;
						for (Reserva r : reservasGrupo) {
							if (r.getId() != reserva.getId()) {
								if (r.getPromedio() < menorNotaCorte) {
									menorNotaCorte = r.getPromedio();
								}
							}
						}
						if (menorNotaCorte == Float.MAX_VALUE) {
							menorNotaCorte = 0f;
						}
						grupo.setNotaCorte(menorNotaCorte);
					}
				grupoDAO.save(grupo);
				} else {
					return false;
				}
			}
		}
		return true;
	}

	public void deleteReservaPreasignada(Reserva reserva) {
		actualizarGrupo(reserva, EstadoReserva.PREASIGNADA, false);
		actualizarGrupo(reserva, EstadoReserva.SOLICITUD, true);
		dao.save(reserva);
	}

}
