/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: Grupo.java
 * Creado: 21-nov-2011
 * Version: 1.0
 * Descripcion: Entidad para la consultad de los grupos de la universidad.
 */
package es.ujaen.pfc.reserva.model.objects;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import es.ujaen.pfc.reserva.web.view.FechasUtil;

@Entity
public class Grupo implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "GRUPO_ID")
	private Long id;
	@ManyToOne
	private Actividad actividad;
	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "grupos")
	private List<Profesor> profesores;
	@OneToMany(mappedBy = "grupo", cascade = CascadeType.ALL)
	private List<Periodo> periodos;
	private int numeroPlazasOcupadas;
	private int numeroPlazas;
	private int numeroReservasSolicitadas;
	private float notaCorte;
	private float notaCorteReservas;
	@Enumerated
	private EstadoGrupo estado;

	public Grupo() {
		super();
		profesores = new ArrayList<Profesor>();
		periodos = new ArrayList<Periodo>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Actividad getActividad() {
		return actividad;
	}

	public void setActividad(Actividad actividad) {
		this.actividad = actividad;
	}

	public List<Profesor> getProfesores() {
		return profesores;
	}

	public void setProfesores(List<Profesor> profesores) {
		this.profesores = profesores;
	}

	public void addProfesor(Profesor profesor) {
		this.profesores.add(profesor);
	}
	
	public List<Periodo> getPeriodos() {
		return periodos;
	}

	public void setPeriodos(List<Periodo> periodos) {
		this.periodos = periodos;
	}

	public void addPeriodo(Periodo periodo) {
		this.periodos.add(periodo);
	}
	
	public int getNumeroPlazasOcupadas() {
		return numeroPlazasOcupadas;
	}

	public void setNumeroPlazasOcupadas(int numeroPlazasOcupadas) {
		this.numeroPlazasOcupadas = numeroPlazasOcupadas;
	}

	public int getNumeroPlazas() {
		return numeroPlazas;
	}

	public void setNumeroPlazas(int numeroPlazas) {
		this.numeroPlazas = numeroPlazas;
	}

	public float getNotaCorte() {
		return notaCorte;
	}

	public void setNotaCorte(float notaCorte) {
		this.notaCorte = notaCorte;
	}

	public EstadoGrupo getEstado() {
		return estado;
	}

	public void setEstado(EstadoGrupo estado) {
		this.estado = estado;
	}

	public boolean isEnConflicto(Grupo grupo) {
		// Calculamos si coincide alguna hora de entre todos los periodos
		// posibles y todas sus sesiones
		for (Periodo p : this.periodos) {
			for (Periodo p2 : grupo.periodos) {
				// Comprobamos si los periodos coinciden
				if (FechasUtil.coincidenFechas(p.getFechaInicio(),
						p.getFechaFin(), p2.getFechaInicio(), p2.getFechaFin())) {
					// Comprobamos entonces si coincide alguna sesion del
					// periodo
					for (SesionLectiva s : p.getSesiones()) {
						for (SesionLectiva s2 : p2.getSesiones()) {
							if (FechasUtil.coincidenFechas(s.getFechaInicio(),
									s.getFechaFin(), s2.getFechaInicio(),
									s2.getFechaFin())) {
								return true;
							}
							if (FechasUtil.coincidenFechas(s2.getFechaInicio(),
									s2.getFechaFin(), s.getFechaInicio(),
									s.getFechaFin())) {
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	public int getNumeroReservasSolicitadas() {
		return numeroReservasSolicitadas;
	}

	public void setNumeroReservasSolicitadas(int numeroReservasSolicitadas) {
		this.numeroReservasSolicitadas = numeroReservasSolicitadas;
	}

	public float getNotaCorteReservas() {
		return notaCorteReservas;
	}

	public void setNotaCorteReservas(float notaCorteReservas) {
		this.notaCorteReservas = notaCorteReservas;
	}

	
}
