/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: ReservaExternoDAO.java
 * Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

import es.ujaen.pfc.reserva.model.objects.Reserva;

// TODO: Auto-generated Javadoc
/**
 * The Interface ReservaExternoDAO.
 */
public interface ReservaDAO extends GenericDAO<Reserva, Long>{


}