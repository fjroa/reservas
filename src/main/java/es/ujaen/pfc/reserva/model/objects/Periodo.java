/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: Periodo.java
 * Creado: 21-nov-2011
 * Version: 1.0
 * Descripcion: Entidad para la consultad de los grupos de la universidad.
 */
package es.ujaen.pfc.reserva.model.objects;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Periodo implements java.io.Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Date fechaInicio;
	private Date fechaFin;
	@ManyToOne
	private Grupo grupo;

	@OneToMany(mappedBy = "periodo", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	private List<SesionLectiva> sesiones;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Periodo() {
		super();
		sesiones = new ArrayList<SesionLectiva>();
	}

	public Periodo(Date fechaInicio, Date fechaFin, List<SesionLectiva> sesiones) {
		super();
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.sesiones = sesiones;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}
	
	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public List<SesionLectiva> getSesiones() {
		return sesiones;
	}

	public void setSesiones(List<SesionLectiva> sesiones) {
		this.sesiones = sesiones;
	}

	public void addSesion(SesionLectiva sesion) {
		this.addSesion(sesion);
	}
}
