package es.ujaen.pfc.reserva.model.objects;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import es.ujaen.pfc.reserva.web.view.Dia;

@Entity
public class SesionLectiva implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@ManyToOne
	private Periodo periodo;
	@Enumerated
	Dia dia;
	int horaInicio;
	int minutoInicio;
	int horaDuracion;
	int minutoDuracion;
	

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Periodo getPeriodo() {
		return periodo;
	}
	public void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}
	public Dia getDia() {
		return dia;
	}
	public void setDia(Dia dia) {
		this.dia = dia;
	}
	public int getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(int horaInicio) {
		this.horaInicio = horaInicio;
	}
	public int getMinutoInicio() {
		return minutoInicio;
	}
	public void setMinutoInicio(int minutoInicio) {
		this.minutoInicio = minutoInicio;
	}
	public int getHoraDuracion() {
		return horaDuracion;
	}
	public void setHoraDuracion(int horaDuracion) {
		this.horaDuracion = horaDuracion;
	}
	public int getMinutoDuracion() {
		return minutoDuracion;
	}
	public void setMinutoDuracion(int minutoDuracion) {
		this.minutoDuracion = minutoDuracion;
	}
	public SesionLectiva() {
		super();
	}
	
	public SesionLectiva(Dia dia, int horaInicio, int minutoInicio,
			int horaDuracion, int minutoDuracion) {
		super();
		this.dia = dia;
		this.horaInicio = horaInicio;
		this.minutoInicio = minutoInicio;
		this.horaDuracion = horaDuracion;
		this.minutoDuracion = minutoDuracion;
	}
	public Date getFechaInicio() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.HOUR, horaInicio);
		cal.set(Calendar.MINUTE, minutoInicio);
		cal.set(Calendar.DAY_OF_WEEK, dia.getValor());
		return cal.getTime();
	}
	public Date getFechaFin() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.HOUR, horaInicio+horaDuracion);
		cal.set(Calendar.MINUTE, minutoInicio+minutoDuracion);
		cal.set(Calendar.DAY_OF_WEEK, dia.getValor());
		return cal.getTime();
	}

	
	
}
