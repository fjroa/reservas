/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: UsuarioExternoService.java
* Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.services;

import java.util.List;

import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.SearchResult;

import es.ujaen.pfc.reserva.model.objects.Usuario;

public interface UsuarioService {

	public void save(Usuario profesor);

	public void delete(Usuario profesor);

	public List<Usuario> findAll();
	
	public List<Usuario> search(ISearch search);

	public SearchResult<Usuario> searchAndCount(ISearch search);
	
	public Usuario findById(String idUsuario);

	public Usuario findByNombre(String nombre);
	
	public void flush();

}

