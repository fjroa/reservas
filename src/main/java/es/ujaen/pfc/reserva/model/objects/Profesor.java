package es.ujaen.pfc.reserva.model.objects;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
@DiscriminatorValue("Profesor")
public class Profesor extends Usuario {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8377302713839248717L;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name = "PROFESOR_GRUPOS",
			joinColumns = {@JoinColumn(name = "PROFESOR_ID")},
			inverseJoinColumns = {@JoinColumn(name = "GRUPO_ID")}
	)
	private List<Grupo> grupos;
	
	public Profesor() {
		super();
		grupos = new ArrayList<Grupo>();
	}

	public Profesor(List<Grupo> grupos) {
		super();
		this.grupos = grupos;
	}

	public List<Grupo> getGrupos() {
		return grupos;
	}

	public void setGrupos(List<Grupo> grupos) {
		this.grupos = grupos;
	}

	public void addGrupo(Grupo grupo) {
		this.grupos.add(grupo);
	}
	
	
}
