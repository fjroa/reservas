/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: Usuario.java
 * Creado: 21-nov-2011
 * Version: 1.0
 * Descripcion: Entidad para la consultad de los datos de usuarios de la universidad.
 */
package es.ujaen.pfc.reserva.model.objects;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;


@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
		name = "ROL",
		discriminatorType = DiscriminatorType.STRING
)
@DiscriminatorValue("Usuario")
public class Usuario implements java.io.Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	@Id
    @Column(name="USUARIO_ID")
	private String idUsuario;
	private String password;
	private String nombre;
	private String email;
	@ManyToMany
	@JoinTable(
			name = "USUARIO_ACTIVIDADES",
			joinColumns = {@JoinColumn(name = "USUARIO_ID")},
			inverseJoinColumns = {@JoinColumn(name = "ACTIVIDAD_ID")}
	)
	private List<Actividad> actividades;
	
	public Usuario() {
		super();
		actividades = new ArrayList<Actividad>();
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}


	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<Actividad> getActividades() {
		return actividades;
	}
	public void setActividades(List<Actividad> actividades) {
		this.actividades = actividades;
	}

	public void addActividad(Actividad a) {
		actividades.add(a);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
