package es.ujaen.pfc.reserva.model.objects;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
@DiscriminatorValue("Alumno")
public class Alumno extends Usuario {

	private static final long serialVersionUID = -5113326119535616083L;
	
	private float promedio; 
	@ManyToMany
	@JoinTable(
			name = "ALUMNO_GRUPOSTEORIA",
			joinColumns = {@JoinColumn(name = "ALUMNO_ID")},
			inverseJoinColumns = {@JoinColumn(name = "GRUPO_ID")}
	)
	private List<Grupo> gruposTeoria;

	public Alumno() {
		super();
		gruposTeoria = new ArrayList<Grupo>();
	}

	public float getPromedio() {
		return promedio;
	}

	public void setPromedio(float promedio) {
		this.promedio = promedio;
	}

	public List<Grupo> getGruposTeoria() {
		return gruposTeoria;
	}

	public void setGruposTeoria(List<Grupo> gruposTeoria) {
		this.gruposTeoria = gruposTeoria;
	}
	
	public void addGrupoTeoria(Grupo grupoTeoria) {
		this.gruposTeoria.add(grupoTeoria);
	}
}
