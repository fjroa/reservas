/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: DepartamentoExternoDAO.java
 * Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

import es.ujaen.pfc.reserva.model.objects.Centro;

// TODO: Auto-generated Javadoc
/**
 * The Interface DepartamentoExternoDAO.
 */
public interface CentroDAO extends GenericDAO<Centro, String>{


}