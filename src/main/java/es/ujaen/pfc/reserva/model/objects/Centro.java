package es.ujaen.pfc.reserva.model.objects;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("Centro")
public class Centro extends Usuario {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7945509942758493663L;
	
	@OneToMany(mappedBy = "centro", cascade = CascadeType.ALL)
	private List<Departamento> departamentos;
	
	public Centro() {
		super();
		this.departamentos = new ArrayList<Departamento>();
	}

	public List<Departamento> getDepartamentos() {
		return departamentos;
	}

	public void setDepartamentos(List<Departamento> departamentos) {
		this.departamentos = departamentos;
	}
	
	public void addDepartamento(Departamento departamento) {
		this.departamentos.add(departamento);
	}
}
