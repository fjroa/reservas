package es.ujaen.pfc.reserva.model.objects;

public enum EstadoGrupo {
	ABIERTO, CERRADO, TEORIA, LLENO, LIBRE, CONFLICTO, NO_SELECCIONABLE, SELECCIONADO;
}
