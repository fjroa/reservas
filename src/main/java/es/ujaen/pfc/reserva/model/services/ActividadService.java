/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: ActividadExternoService.java
* Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.services;

import java.util.List;

import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.SearchResult;

import es.ujaen.pfc.reserva.model.objects.Actividad;
import es.ujaen.pfc.reserva.model.objects.Usuario;

public interface ActividadService {

	public void save(Actividad actividad);

	public void delete(Actividad actividad);

	public List<Actividad> findAll();
	
	public List<Actividad> search(ISearch search);

	public SearchResult<Actividad> searchAndCount(ISearch search);
		
	public Actividad findById(long id);

	public Actividad findByNombre(String nombre);
	
	public Actividad findByAbreviatura(String abreviatura);
	
	public List<Actividad> findByCoordinador(Usuario usuario);
	
	public void flush();

}
