/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: CentroExternoService.java
 * Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.services;

import java.util.List;

import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.SearchResult;

import es.ujaen.pfc.reserva.model.objects.Actividad;
import es.ujaen.pfc.reserva.model.objects.Centro;

public interface CentroService {

	public void save(Centro centro);

	public void delete(Centro centro);

	public List<Centro> findAll();

	public List<Centro> search(ISearch search);

	public SearchResult<Centro> searchAndCount(ISearch search);

	public Centro findById(String idUsuario);

	public Centro findByNombre(String nombre);

	public void flush();

	public List<Centro> findByActividad(Actividad actividad);

}
