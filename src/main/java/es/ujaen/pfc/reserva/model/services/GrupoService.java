/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: GrupoExternoService.java
* Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.services;

import java.util.List;

import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.SearchResult;

import es.ujaen.pfc.reserva.model.objects.Actividad;
import es.ujaen.pfc.reserva.model.objects.EstadoGrupo;
import es.ujaen.pfc.reserva.model.objects.Grupo;
import es.ujaen.pfc.reserva.model.objects.Profesor;

public interface GrupoService {

	public void save(Grupo grupo);

	public void delete(Grupo grupo);

	public List<Grupo> findAll();
	
	public List<Grupo> search(ISearch search);

	public SearchResult<Grupo> searchAndCount(ISearch search);
	
	public Grupo findById(long id);
	
	public List<Grupo> findByActividad(Actividad actividad);
	
	public List<Grupo> findByProfesor(Profesor profesor);
	
	public List<Grupo> findByEstado(EstadoGrupo estado);
	
}
