/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: ReservaExternoService.java
* Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.services;

import java.util.Date;
import java.util.List;

import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.SearchResult;

import es.ujaen.pfc.reserva.model.objects.Alumno;
import es.ujaen.pfc.reserva.model.objects.EstadoReserva;
import es.ujaen.pfc.reserva.model.objects.Grupo;
import es.ujaen.pfc.reserva.model.objects.Reserva;

public interface ReservaService {

	public void save(Reserva reserva);

	public void delete(Reserva reserva);

	public List<Reserva> findAll();
	
	public List<Reserva> search(ISearch search);

	public SearchResult<Reserva> searchAndCount(ISearch search);
	
	public Reserva findById(long codigo);
	
	public List<Reserva> findByAlumno(Alumno alumno);

	public List<Reserva> findAsignadasByGrupo(Grupo grupo);
	
	public List<Reserva> findSolicitudesByGrupo(Grupo grupo);
	
	public List<Reserva> findByGrupo(Grupo grupo);

	public List<Reserva> findByFechas(Date fechaInicio, Date fechaFin);
	
	public List<Reserva> findByEstadoReserva(EstadoReserva estado);

	public Reserva findByAlumnoGrupo(Alumno alumno, Grupo grupo);

	public void saveReservaPreasignada(Reserva reserva);
	
	public void deleteReservaPreasignada(Reserva reserva);
	
}
