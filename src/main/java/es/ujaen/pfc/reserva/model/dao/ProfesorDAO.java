/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: ProfesorExternoDAO.java
 * Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

import es.ujaen.pfc.reserva.model.objects.Profesor;

// TODO: Auto-generated Javadoc
/**
 * The Interface ProfesorExternoDAO.
 */
public interface ProfesorDAO extends GenericDAO<Profesor, String>{


}