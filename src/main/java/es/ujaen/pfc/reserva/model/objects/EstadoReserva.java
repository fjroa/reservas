package es.ujaen.pfc.reserva.model.objects;

public enum EstadoReserva {
	CREACION,
	SOLICITUD,
	PREASIGNADA,
	ASIGNADA,
	ELIMINADA;
}
