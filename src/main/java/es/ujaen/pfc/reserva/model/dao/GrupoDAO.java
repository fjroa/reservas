/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: GrupoExternoDAO.java
 * Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

import es.ujaen.pfc.reserva.model.objects.Grupo;

// TODO: Auto-generated Javadoc
/**
 * The Interface GrupoExternoDAO.
 */
public interface GrupoDAO extends GenericDAO<Grupo, Long>{


}