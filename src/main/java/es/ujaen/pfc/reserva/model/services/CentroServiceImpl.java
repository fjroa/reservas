/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: CentroExternoServiceImpl.java
 * Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.Search;
import com.googlecode.genericdao.search.SearchResult;

import es.ujaen.pfc.reserva.model.dao.CentroDAO;
import es.ujaen.pfc.reserva.model.objects.Actividad;
import es.ujaen.pfc.reserva.model.objects.Centro;

@Service
public class CentroServiceImpl implements CentroService {

	CentroDAO dao;

	@Autowired
	public void setDao(CentroDAO dao) {
		this.dao = dao;
	}

	public void save(Centro Centro) {
		dao.save(Centro);
	}

	public void delete(Centro Centro) {
		dao.removeById(Centro.getIdUsuario());
	}

	public List<Centro> findAll() {
		return dao.findAll();
	}

	public List<Centro> search(ISearch search) {
		return dao.search(search);
	}

	public SearchResult<Centro> searchAndCount(ISearch search) {
		return dao.searchAndCount(search);
	}

	public Centro findById(String idUsuario) {
		return dao.find(idUsuario);
	}

	public Centro findByNombre(String nombre) {
		if (nombre == null)
			return null;
		return dao.searchUnique(new Search().addFilterEqual("nombre", nombre));
	}

	public void flush() {
		dao.flush();
	}

	public List<Centro> findByActividad(Actividad actividad) {
		return dao.search(new Search().addFilterSome("actividades",
				Filter.equal("codigo", actividad.getCodigo())));
	}

}
