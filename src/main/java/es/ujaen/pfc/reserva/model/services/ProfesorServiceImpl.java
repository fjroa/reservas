/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: ProfesorExternoServiceImpl.java
* Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.Search;
import com.googlecode.genericdao.search.SearchResult;

import es.ujaen.pfc.reserva.model.dao.ProfesorDAO;
import es.ujaen.pfc.reserva.model.objects.Profesor;

@Service
public class ProfesorServiceImpl implements ProfesorService {

ProfesorDAO dao;
	
	@Autowired
	public void setDao(ProfesorDAO dao) {
		this.dao = dao;
	}
	
	public void save(Profesor Profesor){
		dao.save(Profesor);
	}

	public void delete(Profesor Profesor){
		dao.removeById(Profesor.getIdUsuario());
	}

	public List<Profesor> findAll() {
		return dao.findAll();
	}

	public List<Profesor> search(ISearch search) {
		return dao.search(search);
	}

	public SearchResult<Profesor> searchAndCount(ISearch search) {
		return dao.searchAndCount(search);
	}

	public Profesor findById(String idUsuario) {
		return dao.find(idUsuario);
	}

	public Profesor findByNombre(String nombre) {
		if (nombre == null)
			return null;
		return dao.searchUnique(new Search().addFilterEqual("nombre", nombre));
	}

	public void flush() {
		dao.flush();
	}


}
