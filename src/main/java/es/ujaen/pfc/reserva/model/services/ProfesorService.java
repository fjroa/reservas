/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: ProfesorExternoService.java
* Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.services;

import java.util.List;

import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.SearchResult;

import es.ujaen.pfc.reserva.model.objects.Profesor;

public interface ProfesorService {

	public void save(Profesor profesor);

	public void delete(Profesor profesor);

	public List<Profesor> findAll();
	
	public List<Profesor> search(ISearch search);

	public SearchResult<Profesor> searchAndCount(ISearch search);
	
	public Profesor findById(String idUsuario);

	public Profesor findByNombre(String nombre);
	
	public void flush();

}

