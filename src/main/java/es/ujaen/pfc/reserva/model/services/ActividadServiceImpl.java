/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: ActividadExternoServiceImpl.java
* Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.Search;
import com.googlecode.genericdao.search.SearchResult;

import es.ujaen.pfc.reserva.model.dao.ActividadDAO;
import es.ujaen.pfc.reserva.model.objects.Actividad;
import es.ujaen.pfc.reserva.model.objects.Usuario;

@Service
public class ActividadServiceImpl implements ActividadService {

	ActividadDAO dao;
	
	@Autowired
	public void setDao(ActividadDAO dao) {
		this.dao = dao;
	}
	
	public void save(Actividad actividad) {
		dao.save(actividad);
	}

	public void delete(Actividad actividad) {
		dao.removeById(actividad.getCodigo());
	}

	public List<Actividad> findAll() {
		return dao.findAll();
	}

	public List<Actividad> search(ISearch search) {
		return dao.search(search);
	}

	public SearchResult<Actividad> searchAndCount(ISearch search) {
		return dao.searchAndCount(search);
	}

	public Actividad findById(long codigo) {
		return dao.find(codigo);
	}

	public Actividad findByNombre(String nombre) {
		if (nombre == null)
			return null;
		return dao.searchUnique(new Search().addFilterEqual("nombre", nombre));
	}

	public Actividad findByAbreviatura(String abreviatura) {
		if (abreviatura == null)
			return null;
		return dao.searchUnique(new Search().addFilterEqual("abreviatura", abreviatura));
	}

	public List<Actividad> findByCoordinador(Usuario usuario) {
		return dao.search(new Search().addFilterEqual("coordinador", usuario));
	}

	public void flush() {
		dao.flush();
	}
}
