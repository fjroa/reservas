/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: UsuarioDAOImpl.java
* Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.dao;

import org.springframework.stereotype.Repository;

import es.ujaen.pfc.reserva.model.objects.Usuario;

@Repository
public class UsuarioDAOImpl extends BaseDAO<Usuario, String> implements UsuarioDAO {

}