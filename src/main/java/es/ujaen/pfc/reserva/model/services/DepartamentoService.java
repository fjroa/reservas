/**
 * Autor: Francisco Javier Roa Lopez 
 * Archivo: DepartamentoExternoService.java
 * Creado: 13-dic-2012
 * Version: 1.0
 * Descripcion:
 */
package es.ujaen.pfc.reserva.model.services;

import java.util.List;

import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.SearchResult;

import es.ujaen.pfc.reserva.model.objects.Actividad;
import es.ujaen.pfc.reserva.model.objects.Departamento;

public interface DepartamentoService {

	public void save(Departamento departamento);

	public void delete(Departamento departamento);

	public List<Departamento> findAll();

	public List<Departamento> search(ISearch search);

	public SearchResult<Departamento> searchAndCount(ISearch search);

	public Departamento findById(String idUsuario);

	public Departamento findByNombre(String nombre);

	public void flush();

	public List<Departamento> findByActividad(Actividad actividad);

}
