package es.ujaen.pfc.reserva.web;

import java.net.BindException;
import java.security.Principal;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.ujaen.pfc.reserva.model.objects.AccionReserva;
import es.ujaen.pfc.reserva.model.objects.Alumno;
import es.ujaen.pfc.reserva.model.objects.EstadoReserva;
import es.ujaen.pfc.reserva.model.objects.Grupo;
import es.ujaen.pfc.reserva.model.objects.HistoricoReserva;
import es.ujaen.pfc.reserva.model.objects.Reserva;
import es.ujaen.pfc.reserva.model.services.AlumnoService;
import es.ujaen.pfc.reserva.model.services.GrupoService;
import es.ujaen.pfc.reserva.model.services.ReservaService;
import es.ujaen.pfc.reserva.model.services.UsuarioService;

@Controller
@RequestMapping("/grupos")
public class GrupoController {
	private static Log log = LogFactory.getLog(GrupoController.class);

	@Inject
	private GrupoService grupoService;
	@Inject
	private ReservaService reservaService;
	@Inject
	private AlumnoService alumnoService;
	@Inject
	private UsuarioService usuarioService;

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Grupo createGrupo(@Valid Grupo grupo, BindingResult result,
			HttpServletResponse response) throws BindException {
		log.debug("Creamos nuevo grupo");
		if (result.hasErrors()) {
			throw new BindException();
		}
		grupoService.save(grupo);
		log.debug("Grupo creado con id:" + grupo.getId());

		response.setHeader("Location", "/grupos/" + grupo.getId());
		return grupo;
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String getGrupo(@PathVariable long id, Model model) {
		log.debug("Obtenemos grupo con id:" + id);
		model.addAttribute("grupo", grupoService.findById(id));
		return "grupos/item";
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteGrupo(@PathVariable long id) {
		log.debug("Eliminamos grupo con id:" + id);
		grupoService.delete(grupoService.findById(id));
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateGrupo(@PathVariable long id) {
		log.debug("Actualizamos grupo con id:" + id);
		grupoService.save(grupoService.findById(id));
	}

	@PreAuthorize("!hasRole('Alumno')")
	@RequestMapping(value = "/{id}/info", method = RequestMethod.GET)
	public String showInfo(@PathVariable long id, Model model,
			@ModelAttribute("infoApp") final String infoApp,
			@ModelAttribute("errorForm") final String errorForm) {
		log.debug("Obtenemos grupo con id:" + id);
		Grupo g = grupoService.findById(id);

		List<Reserva> plazasAsignadas = reservaService.findAsignadasByGrupo(g);
		List<Reserva> reservasSolicitadas = reservaService
				.findSolicitudesByGrupo(g);

		model.addAttribute("grupo", g);
		model.addAttribute("plazasAsignadas", plazasAsignadas);
		model.addAttribute("reservasSolicitadas", reservasSolicitadas);
		if (errorForm != null && !errorForm.equals("")) {
			model.addAttribute("errorForm", errorForm);
		}
		if (infoApp != null && !infoApp.equals("")) {
			model.addAttribute("infoApp", infoApp);
		}
		return "grupos/info";
	}

	@RequestMapping(value = "/{id}/crearReserva", method = RequestMethod.GET)
	public String formCrearReserva(@PathVariable long id, Model model) {
		log.debug("Preparamos la creacion de reserva en el grupo con id:" + id);
		Grupo g = grupoService.findById(id);

		List<Grupo> gruposActividad = g.getActividad().getGrupos();
		List<Alumno> alumnosDisponibles = alumnoService.findByActividad(g
				.getActividad());

		for (Grupo grupo : gruposActividad) {
			List<Reserva> reservasActividad = reservaService.findByGrupo(grupo);
			for (Reserva r : reservasActividad) {
				alumnosDisponibles.remove(r.getAlumno());
			}
		}

		model.addAttribute("grupo", g);
		model.addAttribute("alumnosDisponibles", alumnosDisponibles);

		return "grupos/crearReserva";
	}

	@RequestMapping(value = "/{id}/crearReserva", method = RequestMethod.POST)
	public String crearReserva(@PathVariable long id, Principal principal,
			HttpServletRequest request) {
		Grupo g = grupoService.findById(id);

		log.info("Creamos reserva nueva...");
		Alumno alumno = alumnoService.findById(request
				.getParameter("alumnoReserva"));
		Reserva reserva = new Reserva();
		reserva.setAlumno(alumno);
		reserva.setPromedio(alumno.getPromedio());
		reserva.setEstadoReserva(EstadoReserva.SOLICITUD);
		HistoricoReserva hr = new HistoricoReserva();
		hr.setAccionReserva(AccionReserva.CREACION);
		hr.setFechaAccion(new Date());
		hr.setUsuario(usuarioService.findById(principal.getName()));
		hr.setEstadoReserva(reserva.getEstadoReserva());
		hr.setComentario(request.getParameter("comentarioCreador"));
		hr.setReserva(reserva);
		reserva.setGrupo(g);
		reserva.addHistoricoReserva(hr);
		reservaService.save(reserva);

		return "redirect:/grupos/" + g.getId() + "/info";
	}

	@RequestMapping(value = "/{id}/actualizarPlazas", method = RequestMethod.POST)
	public String actualizarPlazas(@PathVariable long id,
			final RedirectAttributes redirectAttrs, HttpServletRequest request) {
		Grupo g = grupoService.findById(id);

		log.info("Actualizamos numero de plazas...");
		String valorFormulario = request.getParameter("numeroPlazas");
		try {
			int numeroPlazas = Integer.parseInt(valorFormulario);
			if (numeroPlazas < g.getNumeroPlazasOcupadas()) {
				throw new Exception();
			} else {
				g.setNumeroPlazas(numeroPlazas);
				grupoService.save(g);
				redirectAttrs.addAttribute("errorForm", "");
			}
		} catch (Exception e) {
			redirectAttrs.addAttribute("errorForm", "true");
			return "redirect:/grupos/" + g.getId() + "/info";
		}
		redirectAttrs.addAttribute("infoApp", "true");
		return "redirect:/grupos/" + g.getId() + "/info";
	}
}
