package es.ujaen.pfc.reserva.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import es.ujaen.pfc.reserva.model.objects.AccionReserva;
import es.ujaen.pfc.reserva.model.objects.Actividad;
import es.ujaen.pfc.reserva.model.objects.Alumno;
import es.ujaen.pfc.reserva.model.objects.Centro;
import es.ujaen.pfc.reserva.model.objects.Departamento;
import es.ujaen.pfc.reserva.model.objects.EstadoGrupo;
import es.ujaen.pfc.reserva.model.objects.EstadoReserva;
import es.ujaen.pfc.reserva.model.objects.Grupo;
import es.ujaen.pfc.reserva.model.objects.HistoricoReserva;
import es.ujaen.pfc.reserva.model.objects.Periodo;
import es.ujaen.pfc.reserva.model.objects.Profesor;
import es.ujaen.pfc.reserva.model.objects.Reserva;
import es.ujaen.pfc.reserva.model.objects.SesionLectiva;
import es.ujaen.pfc.reserva.model.objects.Usuario;
import es.ujaen.pfc.reserva.model.services.ActividadService;
import es.ujaen.pfc.reserva.model.services.AlumnoService;
import es.ujaen.pfc.reserva.model.services.CentroService;
import es.ujaen.pfc.reserva.model.services.DepartamentoService;
import es.ujaen.pfc.reserva.model.services.GrupoService;
import es.ujaen.pfc.reserva.model.services.ProfesorService;
import es.ujaen.pfc.reserva.model.services.ReservaService;
import es.ujaen.pfc.reserva.model.services.UsuarioService;
import es.ujaen.pfc.reserva.web.view.Dia;
import es.ujaen.pfc.reserva.web.view.FechasUtil;

@Controller
public class HomeController {

	private static Log log = LogFactory.getLog(HomeController.class);

	@Inject
	private UsuarioService usuarioService;

	@Inject
	private AlumnoService alumnoService;

	@Inject
	private ProfesorService profesorService;

	@Inject
	private ActividadService actividadService;

	@Inject
	private GrupoService grupoService;

	@Inject
	private ReservaService reservaService;
	
	@Inject
	private DepartamentoService departamentoService;
	
	@Inject
	private CentroService centroService;

	@RequestMapping("/")
	public String showHomePage(HttpServletRequest request) {
		String ret = "login";
		if (request.isUserInRole("Alumno")) {
			ret = "alumno";
		}
		if (request.isUserInRole("Profesor")) {
			ret = "redirect:/profesores/"+request.getUserPrincipal().getName()+"/grupos";
		}
		if (request.isUserInRole("Departamento")) {
			ret = "redirect:/departamentos/"+request.getUserPrincipal().getName()+"/grupos";
		}
		if (request.isUserInRole("Centro")) {
			ret = "centro";
		}
		return ret;
	}

	
	@RequestMapping("/login")
	public String showLogin() {
		return "login";
	}
	
	
	//@PostConstruct
	public void iniciarDB() {

		log.info("CREANDO LA BASE DE DATOS...");
		log.info("Creando Actividades");
		Actividad act = new Actividad();
		act.setCodigo(1);
		act.setNombre("Asignatura 1");
		act.setAbreviatura("ASI1");
		actividadService.save(act);

		Actividad act1 = new Actividad();
		act1.setCodigo(2);
		act1.setNombre("Asignatura 2");
		act1.setAbreviatura("ASI2");
		actividadService.save(act1);

		Actividad act2 = new Actividad();
		act2.setCodigo(3);
		act2.setNombre("Asignatura 3");
		act2.setAbreviatura("ASI3");
		actividadService.save(act2);

		log.info("Creando Usuarios...");
		Alumno usr = new Alumno();
		usr.setIdUsuario("ALUM1");
		usr.setPassword("ALUM1");
		usr.setNombre("Alumno 1");
		usr.setEmail("fcojavier.roa@gmail.com");
		usr.setPromedio(1.8f);
		usr.setActividades(actividadService.findAll());
		alumnoService.save(usr);

		usr = new Alumno();
		usr.setIdUsuario("ALUM2");
		usr.setPassword("ALUM2");
		usr.setNombre("Alumno 2");
		usr.setEmail("fcojavier.roa@gmail.com");
		usr.setPromedio(2.5f);
		usr.setActividades(actividadService.findAll());
		alumnoService.save(usr);

		usr = new Alumno();
		usr.setIdUsuario("fjroa");
		usr.setPassword("fjroa");
		usr.setNombre("Francisco Javier Roa L�pez");
		usr.setEmail("fcojavier.roa@gmail.com");
		usr.setPromedio(3.1f);
		usr.setActividades(actividadService.findAll());
		alumnoService.save(usr);

		Profesor prof1 = new Profesor();
		prof1.setIdUsuario("PROFA");
		prof1.setPassword("PROFA");
		prof1.setNombre("Profesor A");
		prof1.setEmail("fcojavier.roa@gmail.com");
		prof1.setActividades(actividadService.findAll());
		profesorService.save(prof1);

		Profesor prof2 = new Profesor();
		prof2.setIdUsuario("PROFB");
		prof2.setPassword("PROFB");
		prof2.setNombre("Profesor B");
		prof2.setActividades(actividadService.findAll());
		prof2.setEmail("fcojavier.roa@gmail.com");

		profesorService.save(prof2);

		Departamento dpto = new Departamento();
		dpto.setIdUsuario("DPTO1");
		dpto.setNombre("Departamento 1");
		dpto.setPassword("DPTO1");
		dpto.setActividades(actividadService.findAll());

		
		Centro centro = new Centro();
		centro.setIdUsuario("CENTRO1");
		centro.setNombre("Centro 1");
		centro.setPassword("CENTRO1");
		centro.setActividades(actividadService.findAll());
		centro.addDepartamento(dpto);
		dpto.setCentro(centro);
		centroService.save(centro);
		
		log.info("Asociando coordinadores a las asignaturas...");
		Actividad a = actividadService.findById(1);
		Usuario usr1 = usuarioService.findById("PROFA");
		Usuario usr2 = usuarioService.findById("PROFB");

		a.setCoordinador(usr1);
		actividadService.save(a);

		a = actividadService.findById(2);
		a.setCoordinador(usr1);
		actividadService.save(a);

		a = actividadService.findById(3);
		a.setCoordinador(usr2);
		actividadService.save(a);

		act = actividadService.findByAbreviatura("ASI1");
		act1 = actividadService.findByAbreviatura("ASI2");
		act2 = actividadService.findByAbreviatura("ASI3");

		log.info("Creamos los grupos de teoria");
		Grupo grupoTeoria = new Grupo();
		grupoTeoria.setEstado(EstadoGrupo.TEORIA);
		grupoTeoria.setNumeroPlazasOcupadas(0);
		grupoTeoria.setNumeroPlazas(20);
		grupoTeoria.setNotaCorte(0f);
		grupoTeoria.setActividad(act);

		SesionLectiva sesion1 = new SesionLectiva();
		sesion1.setDia(Dia.LUNES);
		sesion1.setHoraInicio(10);
		sesion1.setMinutoInicio(0);
		sesion1.setHoraDuracion(1);
		sesion1.setMinutoDuracion(30);
		
		Periodo cuatrimestre = new Periodo();
		Date inicioCuatrimestre = FechasUtil.convertStringToDate("14/09/2012",
				FechasUtil.TYPESDFDATE);
		Date finCuatrimestre = FechasUtil.convertStringToDate("11/01/2013",
				FechasUtil.TYPESDFDATE);
		cuatrimestre.setFechaInicio(inicioCuatrimestre);
		cuatrimestre.setFechaFin(finCuatrimestre);
		List<SesionLectiva> listaSesiones = new ArrayList<SesionLectiva>();
		listaSesiones.add(sesion1);
		sesion1.setPeriodo(cuatrimestre);
		cuatrimestre.setSesiones(listaSesiones);

		List<Periodo> listaPeriodos = new ArrayList<Periodo>();
		listaPeriodos.add(cuatrimestre);
		cuatrimestre.setGrupo(grupoTeoria);
		grupoTeoria.setPeriodos(listaPeriodos);
		grupoService.save(grupoTeoria);

		log.info("Matriculamos a los alumnos en las asignaturas");
		Alumno alumno = alumnoService.findById("ALUM1");
		List<Grupo> gruposTeoria = new ArrayList<Grupo>();
		gruposTeoria.add(grupoTeoria);
		alumno.setGruposTeoria(gruposTeoria);
		actividadService.save(act);
		alumnoService.save(alumno);
		
		log.info("Creamos los grupos de practicas...");
		Grupo grupo = new Grupo();
		grupo.setEstado(EstadoGrupo.ABIERTO);
		grupo.setNumeroPlazasOcupadas(0);
		grupo.setNumeroPlazas(20);
		grupo.setNotaCorte(0f);
		grupo.setActividad(act);
		
		SesionLectiva sesion = new SesionLectiva();
		sesion.setDia(Dia.JUEVES);
		sesion.setHoraInicio(11);
		sesion.setMinutoInicio(0);
		sesion.setHoraDuracion(1);
		sesion.setMinutoDuracion(0);
		sesion.setPeriodo(cuatrimestre);
		
		cuatrimestre = new Periodo();
		cuatrimestre.setFechaInicio(inicioCuatrimestre);
		cuatrimestre.setFechaFin(finCuatrimestre);
		listaSesiones = new ArrayList<SesionLectiva>();
		listaSesiones.add(sesion);
		sesion.setPeriodo(cuatrimestre);
		cuatrimestre.setSesiones(listaSesiones);

		listaPeriodos = new ArrayList<Periodo>();
		listaPeriodos.add(cuatrimestre);
		cuatrimestre.setGrupo(grupo);
		grupo.setPeriodos(listaPeriodos);
		
		grupoService.save(grupo);

		Grupo grupo1 = new Grupo();
		grupo1.setEstado(EstadoGrupo.CERRADO);
		grupo1.setNumeroPlazasOcupadas(0);
		grupo1.setNumeroPlazas(20);
		grupo1.setNotaCorte(0f);
		grupo1.setActividad(act);
		sesion = new SesionLectiva();
		sesion.setDia(Dia.JUEVES);
		sesion.setHoraInicio(12);
		sesion.setMinutoInicio(0);
		sesion.setHoraDuracion(1);
		sesion.setMinutoDuracion(0);
		sesion.setPeriodo(cuatrimestre);
		
		cuatrimestre = new Periodo();
		cuatrimestre.setFechaInicio(inicioCuatrimestre);
		cuatrimestre.setFechaFin(finCuatrimestre);
		listaSesiones = new ArrayList<SesionLectiva>();
		listaSesiones.add(sesion);
		sesion.setPeriodo(cuatrimestre);
		cuatrimestre.setSesiones(listaSesiones);

		listaPeriodos = new ArrayList<Periodo>();
		listaPeriodos.add(cuatrimestre);
		cuatrimestre.setGrupo(grupo1);
		grupo1.setPeriodos(listaPeriodos);
		grupoService.save(grupo1);

		Grupo grupo2 = new Grupo();
		grupo2.setEstado(EstadoGrupo.ABIERTO);
		grupo2.setNumeroPlazasOcupadas(0);
		grupo2.setNumeroPlazas(20);
		grupo2.setNotaCorte(0f);
		grupo2.setActividad(act);
		sesion = new SesionLectiva();
		sesion.setDia(Dia.JUEVES);
		sesion.setHoraInicio(18);
		sesion.setMinutoInicio(0);
		sesion.setHoraDuracion(1);
		sesion.setMinutoDuracion(0);
		sesion.setPeriodo(cuatrimestre);
		
		cuatrimestre = new Periodo();
		cuatrimestre.setFechaInicio(inicioCuatrimestre);
		cuatrimestre.setFechaFin(finCuatrimestre);
		listaSesiones = new ArrayList<SesionLectiva>();
		listaSesiones.add(sesion);
		sesion.setPeriodo(cuatrimestre);
		cuatrimestre.setSesiones(listaSesiones);

		listaPeriodos = new ArrayList<Periodo>();
		listaPeriodos.add(cuatrimestre);
		cuatrimestre.setGrupo(grupo2);
		grupo2.setPeriodos(listaPeriodos);
		grupoService.save(grupo2);

		Grupo grupo3 = new Grupo();
		grupo3.setEstado(EstadoGrupo.ABIERTO);
		grupo3.setNumeroPlazasOcupadas(0);
		grupo3.setNumeroPlazas(20);
		grupo3.setNotaCorte(0f);
		grupo3.setActividad(act1);
		sesion = new SesionLectiva();
		sesion.setDia(Dia.JUEVES);
		sesion.setHoraInicio(10);
		sesion.setMinutoInicio(0);
		sesion.setHoraDuracion(2);
		sesion.setMinutoDuracion(0);
		sesion.setPeriodo(cuatrimestre);
		
		cuatrimestre = new Periodo();
		cuatrimestre.setFechaInicio(inicioCuatrimestre);
		cuatrimestre.setFechaFin(finCuatrimestre);
		listaSesiones = new ArrayList<SesionLectiva>();
		listaSesiones.add(sesion);
		sesion.setPeriodo(cuatrimestre);
		cuatrimestre.setSesiones(listaSesiones);

		listaPeriodos = new ArrayList<Periodo>();
		listaPeriodos.add(cuatrimestre);
		cuatrimestre.setGrupo(grupo3);
		grupo3.setPeriodos(listaPeriodos);
		grupoService.save(grupo3);

		Grupo grupo4 = new Grupo();
		grupo4.setEstado(EstadoGrupo.ABIERTO);
		grupo4.setNumeroPlazasOcupadas(0);
		grupo4.setNumeroPlazas(20);
		grupo4.setNotaCorte(0f);
		grupo4.setActividad(act1);
		sesion = new SesionLectiva();
		sesion.setDia(Dia.JUEVES);
		sesion.setHoraInicio(12);
		sesion.setMinutoInicio(0);
		sesion.setHoraDuracion(2);
		sesion.setMinutoDuracion(0);
		sesion.setPeriodo(cuatrimestre);
		
		cuatrimestre = new Periodo();
		cuatrimestre.setFechaInicio(inicioCuatrimestre);
		cuatrimestre.setFechaFin(finCuatrimestre);
		listaSesiones = new ArrayList<SesionLectiva>();
		listaSesiones.add(sesion);
		sesion.setPeriodo(cuatrimestre);
		cuatrimestre.setSesiones(listaSesiones);

		listaPeriodos = new ArrayList<Periodo>();
		listaPeriodos.add(cuatrimestre);
		cuatrimestre.setGrupo(grupo4);
		grupo4.setPeriodos(listaPeriodos);
		grupoService.save(grupo4);

		Grupo grupo5 = new Grupo();
		grupo5.setEstado(EstadoGrupo.ABIERTO);
		grupo5.setNumeroPlazasOcupadas(0);
		grupo5.setNumeroPlazas(20);
		grupo5.setNotaCorte(0f);
		grupo5.setActividad(act2);
		sesion = new SesionLectiva();
		sesion.setDia(Dia.JUEVES);
		sesion.setHoraInicio(11);
		sesion.setMinutoInicio(30);
		sesion.setHoraDuracion(1);
		sesion.setMinutoDuracion(0);
		sesion.setPeriodo(cuatrimestre);
		
		cuatrimestre = new Periodo();
		cuatrimestre.setFechaInicio(inicioCuatrimestre);
		cuatrimestre.setFechaFin(finCuatrimestre);
		listaSesiones = new ArrayList<SesionLectiva>();
		listaSesiones.add(sesion);
		sesion.setPeriodo(cuatrimestre);
		cuatrimestre.setSesiones(listaSesiones);

		listaPeriodos = new ArrayList<Periodo>();
		listaPeriodos.add(cuatrimestre);
		cuatrimestre.setGrupo(grupo5);
		grupo5.setPeriodos(listaPeriodos);
		grupoService.save(grupo5);

		Grupo grupo6 = new Grupo();
		grupo6.setEstado(EstadoGrupo.ABIERTO);
		grupo6.setNumeroPlazasOcupadas(0);
		grupo6.setNumeroPlazas(20);
		grupo6.setNotaCorte(0f);
		grupo6.setActividad(act2);
		sesion = new SesionLectiva();
		sesion.setDia(Dia.JUEVES);
		sesion.setHoraInicio(12);
		sesion.setMinutoInicio(30);
		sesion.setHoraDuracion(1);
		sesion.setMinutoDuracion(0);
		sesion.setPeriodo(cuatrimestre);
		
		cuatrimestre = new Periodo();
		cuatrimestre.setFechaInicio(inicioCuatrimestre);
		cuatrimestre.setFechaFin(finCuatrimestre);
		listaSesiones = new ArrayList<SesionLectiva>();
		listaSesiones.add(sesion);
		sesion.setPeriodo(cuatrimestre);
		cuatrimestre.setSesiones(listaSesiones);

		listaPeriodos = new ArrayList<Periodo>();
		listaPeriodos.add(cuatrimestre);
		cuatrimestre.setGrupo(grupo6);
		grupo6.setPeriodos(listaPeriodos);
		grupoService.save(grupo6);

		log.info("Asignamos profesores a los grupos...");

		prof1 = profesorService.findById("PROFA");
		prof2 = profesorService.findById("PROFB");
		
		
		List<Grupo> listProf1 = new ArrayList<Grupo>();
		listProf1.add(grupo);
		listProf1.add(grupo2);
		listProf1.add(grupo4);
		listProf1.add(grupo5);
		listProf1.add(grupo6);
		List<Grupo> listProf2 = new ArrayList<Grupo>();
		listProf2.add(grupo1);
		listProf2.add(grupo3);
		listProf2.add(grupo5);
		
		prof1.setGrupos(listProf1);
		prof2.setGrupos(listProf2);
		
		profesorService.save(prof1);
		profesorService.save(prof2);
		

		log.info("Creamos reservas de prueba...");
		alumno = alumnoService.findById("ALUM1");
		HistoricoReserva hr = new HistoricoReserva();
		hr.setAccionReserva(AccionReserva.CREACION);
		hr.setFechaAccion(new Date());
		hr.setUsuario(alumno);
		Reserva reserva = new Reserva();
		reserva.setAlumno(alumno);
		reserva.setPromedio(alumno.getPromedio());
		reserva.setEstadoReserva(EstadoReserva.SOLICITUD);
		reserva.addHistoricoReserva(hr);
		reserva.setGrupo(grupo2);
		hr.setEstadoReserva(reserva.getEstadoReserva());
		hr.setReserva(reserva);
		reservaService.save(reserva);

		hr = new HistoricoReserva();
		hr.setAccionReserva(AccionReserva.CREACION);
		hr.setFechaAccion(new Date());
		hr.setUsuario(alumno);		
		Reserva reserva2 = new Reserva();
		reserva2.setAlumno(alumno);
		reserva2.setPromedio(alumno.getPromedio());
		reserva2.setEstadoReserva(EstadoReserva.SOLICITUD);
		reserva2.setGrupo(grupo3);
		hr.setEstadoReserva(reserva2.getEstadoReserva());
		hr.setReserva(reserva2);
		reserva2.addHistoricoReserva(hr);
		reservaService.save(reserva2);

	}
}
