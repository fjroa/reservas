package es.ujaen.pfc.reserva.web;

import java.net.BindException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import es.ujaen.pfc.reserva.model.objects.Actividad;
import es.ujaen.pfc.reserva.model.objects.EstadoGrupo;
import es.ujaen.pfc.reserva.model.objects.Grupo;
import es.ujaen.pfc.reserva.model.objects.Profesor;
import es.ujaen.pfc.reserva.model.services.ActividadService;
import es.ujaen.pfc.reserva.model.services.ProfesorService;
import es.ujaen.pfc.reserva.web.view.ActividadCalendario;
import es.ujaen.pfc.reserva.web.view.GrupoCalendario;

@Controller
@RequestMapping("/profesores")
public class ProfesorController {
	private static Log log = LogFactory.getLog(ProfesorController.class);

	@Inject
	private ProfesorService profesorService;
	@Inject
	private ActividadService actividadService;
	
	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Profesor createProfesor(@Valid Profesor profesor, BindingResult result,
			HttpServletResponse response) throws BindException {
		log.debug("Creamos nuevo profesor");
		if (result.hasErrors()) {
			throw new BindException();
		}
		profesorService.save(profesor);
		log.debug("Profesor creado con id:" + profesor.getIdUsuario());
		
		response.setHeader("Location", "/profesores/"+profesor.getIdUsuario());
		return profesor;
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String getProfesor(@PathVariable String id, Model model) {
		log.debug("Obtenemos profesor con id:" + id);
		model.addAttribute("profesor", profesorService.findById(id));
		return "profesores/item";
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteProfesor(@PathVariable String id) {
		log.debug("Eliminamos profesor con id:" + id);
		profesorService.delete(profesorService.findById(id));
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateProfesor(@PathVariable String id) {
		log.debug("Actualizamos profesor con id:" + id);
		profesorService.save(profesorService.findById(id));
	}

	@PreAuthorize("hasRole('Profesor') and #id == principal.username")
	@RequestMapping(value = "/{id}/grupos", method = RequestMethod.GET)
	public String getGruposProfesor(@PathVariable String id, Model model) {
		log.debug("Obtenemos el listado de grupos para el profesor con id:" + id);

		Profesor profesor = profesorService.findById(id);
		Set<Grupo> grupos = new HashSet<Grupo>();
		for (Grupo g : profesor.getGrupos()) {
			grupos.add(g);
		}
		List<Actividad> actividades = actividadService.findByCoordinador(profesor);
		for (Actividad a : actividades) {
			for (Grupo g: a.getGrupos()) {
				grupos.add(g);
			}
		}
		
		List<ActividadCalendario> actividadesProfesor = new ArrayList<ActividadCalendario>();
		for (Actividad a : profesor.getActividades()) {
			ActividadCalendario ac = new ActividadCalendario(a);
			for (Grupo g : a.getGrupos()) {
				if (!g.getEstado().equals(EstadoGrupo.TEORIA)) {
					boolean grupoPermitido = false;
					for (Grupo g2 : grupos) {
						if (g2.getId().intValue() == g.getId().intValue()) {
							grupoPermitido = true;
						}
					}
					if (!grupoPermitido) {
						g.setEstado(EstadoGrupo.NO_SELECCIONABLE);
					}
					GrupoCalendario gc = new GrupoCalendario(g);
					ac.addGrupo(gc);
				}
			}
			actividadesProfesor.add(ac);
		}
		
		
		model.addAttribute("actividades", actividadesProfesor);

		return "profesores/grupos";
	}
}
