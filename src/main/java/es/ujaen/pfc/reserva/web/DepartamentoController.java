package es.ujaen.pfc.reserva.web;

import java.net.BindException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import es.ujaen.pfc.reserva.model.objects.Actividad;
import es.ujaen.pfc.reserva.model.objects.Departamento;
import es.ujaen.pfc.reserva.model.objects.EstadoGrupo;
import es.ujaen.pfc.reserva.model.objects.Grupo;
import es.ujaen.pfc.reserva.model.services.ActividadService;
import es.ujaen.pfc.reserva.model.services.DepartamentoService;
import es.ujaen.pfc.reserva.web.view.ActividadCalendario;
import es.ujaen.pfc.reserva.web.view.GrupoCalendario;

@Controller
@RequestMapping("/departamentos")
public class DepartamentoController {
	private static Log log = LogFactory.getLog(DepartamentoController.class);

	@Inject
	private DepartamentoService departamentoService;
	@Inject
	private ActividadService actividadService;
	
	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Departamento createDepartamento(@Valid Departamento departamento, BindingResult result,
			HttpServletResponse response) throws BindException {
		log.debug("Creamos nuevo departamento");
		if (result.hasErrors()) {
			throw new BindException();
		}
		departamentoService.save(departamento);
		log.debug("Departamento creado con id:" + departamento.getIdUsuario());
		
		response.setHeader("Location", "/departamentos/"+departamento.getIdUsuario());
		return departamento;
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String getDepartamento(@PathVariable String id, Model model) {
		log.debug("Obtenemos departamento con id:" + id);
		model.addAttribute("departamento", departamentoService.findById(id));
		return "departamentos/item";
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteDepartamento(@PathVariable String id) {
		log.debug("Eliminamos departamento con id:" + id);
		departamentoService.delete(departamentoService.findById(id));
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateDepartamento(@PathVariable String id) {
		log.debug("Actualizamos departamento con id:" + id);
		departamentoService.save(departamentoService.findById(id));
	}

	@PreAuthorize("hasRole('Departamento') and #id == principal.username")
	@RequestMapping(value = "/{id}/grupos", method = RequestMethod.GET)
	public String getGruposDepartamento(@PathVariable String id, Model model) {
		log.debug("Obtenemos el listado de grupos para el departamento con id:" + id);

		Departamento departamento = departamentoService.findById(id);
		
		List<ActividadCalendario> actividadesDepartamento = new ArrayList<ActividadCalendario>();
		for (Actividad a : departamento.getActividades()) {
			ActividadCalendario ac = new ActividadCalendario(a);
			for (Grupo g : a.getGrupos()) {
				if (!g.getEstado().equals(EstadoGrupo.TEORIA)) {
					GrupoCalendario gc = new GrupoCalendario(g);
					ac.addGrupo(gc);
				}
			}
			actividadesDepartamento.add(ac);
		}
		
		
		model.addAttribute("actividades", actividadesDepartamento);

		return "departamentos/grupos";
	}
	
	@RequestMapping(value = "/{id}/report", method = RequestMethod.GET)
	public String generarDataSourceReport(@PathVariable String id, Model model) {
		Departamento departamento = departamentoService.findById(id);
		model.addAttribute("departamentoData", (Collection<Actividad>) departamento.getActividades());
		return ("departamentoReport");
	}
}
