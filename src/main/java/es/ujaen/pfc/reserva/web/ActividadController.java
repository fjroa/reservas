package es.ujaen.pfc.reserva.web;

import java.net.BindException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.ujaen.pfc.reserva.model.objects.Actividad;
import es.ujaen.pfc.reserva.model.objects.EstadoGrupo;
import es.ujaen.pfc.reserva.model.objects.Grupo;
import es.ujaen.pfc.reserva.model.objects.HistoricoReserva;
import es.ujaen.pfc.reserva.model.objects.Reserva;
import es.ujaen.pfc.reserva.model.services.ActividadService;

@Controller
@RequestMapping("/actividades")
public class ActividadController {
	private static Log log = LogFactory.getLog(ActividadController.class);

	@Inject
	private ActividadService actividadService;

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Actividad createActividad(@Valid Actividad actividad, BindingResult result,
			HttpServletResponse response) throws BindException {
		log.debug("Creamos nueva actividad");
		if (result.hasErrors()) {
			throw new BindException();
		}
		actividadService.save(actividad);
		log.debug("Actividad creada con codigo:" + actividad.getCodigo());
		
		response.setHeader("Location", "/actividades/"+actividad.getCodigo());
		return actividad;
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String getActividad(@PathVariable long id, Model model) {
		log.debug("Obtenemos actividad con codigo:" + id);
		model.addAttribute("actividad", actividadService.findById(id));
		return "actividades/item";
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteActividad(@PathVariable long id) {
		log.debug("Eliminamos actividad con codigo:" + id);
		actividadService.delete(actividadService.findById(id));
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateActividad(@PathVariable long id) {
		log.debug("Actualizamos actividad con codigo:" + id);
		actividadService.save(actividadService.findById(id));
	}

	@PreAuthorize("!hasRole('Alumno')")
	@RequestMapping(value = "/{id}/info", method = RequestMethod.GET)
	public String showInfo(@PathVariable long id, Model model,
			@ModelAttribute("infoApp") final String infoApp,
			@ModelAttribute("errorForm") final String errorForm) {
		log.debug("Obtenemos actividad con id:" + id);
		Actividad actividad = actividadService.findById(id);


		model.addAttribute("actividad", actividad);

		if (errorForm != null && !errorForm.equals("")) {
			model.addAttribute("errorForm", errorForm);
		}
		if (infoApp != null && !infoApp.equals("")) {
			model.addAttribute("infoApp", infoApp);
		}
		return "actividades/info";
	}
	
	@RequestMapping(value = "/{id}/actualizarAbreviatura", method = RequestMethod.POST)
	public String actualizarAbreviatura(@PathVariable long id,
			final RedirectAttributes redirectAttrs, HttpServletRequest request) {
		Actividad actividad = actividadService.findById(id);

		log.info("Actualizamos numero de plazas...");
		String valorFormulario = request.getParameter("abreviatura");
		try {
			if (valorFormulario == null || valorFormulario.equals("")) {
				throw new Exception();
			} else {
				actividad.setAbreviatura(valorFormulario);
				actividadService.save(actividad);
				redirectAttrs.addAttribute("errorForm", "");
			}
		} catch (Exception e) {
			redirectAttrs.addAttribute("errorForm", "true");
			return "redirect:/actividades/" + actividad.getCodigo() + "/info";
		}
		redirectAttrs.addAttribute("infoApp", "true");
		return "redirect://actividades/" + actividad.getCodigo() + "/info";
	}
	
	@RequestMapping(value = "/{id}/report", method = RequestMethod.GET)
	public String generarDataSourceReport(@PathVariable Long id, Model model) {
		Actividad actividad = actividadService.findById(id);
		Collection<Actividad> col = new ArrayList<Actividad>();
		col.add(actividad);
		model.addAttribute("actividadData", col);
		Collection<Grupo> subcol = new ArrayList<Grupo>();
		for (Grupo g : actividad.getGrupos()) {
			if (g.getEstado() != EstadoGrupo.TEORIA) {
				subcol.add(g);
			}
		}
		model.addAttribute("SubReportData", subcol);
		return ("asignaturaReport");
	}
}
