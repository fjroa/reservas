package es.ujaen.pfc.reserva.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import es.ujaen.pfc.reserva.model.objects.AccionReserva;
import es.ujaen.pfc.reserva.model.objects.Alumno;
import es.ujaen.pfc.reserva.model.objects.EstadoGrupo;
import es.ujaen.pfc.reserva.model.objects.EstadoReserva;
import es.ujaen.pfc.reserva.model.objects.HistoricoReserva;
import es.ujaen.pfc.reserva.model.objects.Reserva;
import es.ujaen.pfc.reserva.model.services.AlumnoService;
import es.ujaen.pfc.reserva.model.services.GrupoService;
import es.ujaen.pfc.reserva.model.services.ReservaService;
import es.ujaen.pfc.reserva.web.alerts.AlertService;
import es.ujaen.pfc.reserva.web.view.ActividadCalendario;
import es.ujaen.pfc.reserva.web.view.Calendario;
import es.ujaen.pfc.reserva.web.view.GrupoCalendario;

@Component
public class ReservasFlowActions {
	private static Log log = LogFactory.getLog(ReservasFlowActions.class);

	@Inject
	private AlumnoService alumnoService;
	@Inject
	private GrupoService grupoService;
	@Inject
	private ReservaService reservaService;
//	@Inject
//	private AlertService alertService;
	
	public Calendario initCalendario(String userName) {
		Alumno alumno = alumnoService.findById(userName);
		List<Reserva> reservasAlumno = reservaService.findByAlumno(alumno);
		Calendario calendario = new Calendario(alumno, reservasAlumno);
		return calendario;
	}

	public Calendario actualizarAsignaturas(Calendario calendario) {
		for (ActividadCalendario ac : calendario.getListaActividades()) {
			for (GrupoCalendario gc : ac.getGruposCal()) {
				if (!gc.getGrupo().getEstado().equals(EstadoGrupo.TEORIA)) {
					calendario.actualizarGrupo(grupoService.findById(gc
							.getGrupo().getId()));
					// si el grupo esta seleccionado completamos con el estado
					// de la reserva
					if (gc.getGrupo().getEstado()
							.equals(EstadoGrupo.SELECCIONADO)) {
						Reserva r = reservaService.findByAlumnoGrupo(
								calendario.getAlumno(), gc.getGrupo());
						if (r != null) {
							gc.setEstadoReserva(r.getEstadoReserva());

							if (!gc.getEstadoReserva().equals(
									EstadoReserva.SOLICITUD)) {
								calendario.actualizarGrupoAsignado(gc
										.getGrupo());
							}
						}
					}
				}
			}
		}
		return calendario;
	}

	public Calendario addGrupo(Calendario calendario, long idGrupo) {
		calendario.addGrupo(grupoService.findById(idGrupo),
				EstadoReserva.SOLICITUD);
		return calendario;
	}

	public Calendario removeGrupo(Calendario calendario, long idGrupo) {
		calendario.deleteGrupo(grupoService.findById(idGrupo));
		return calendario;
	}

	public void filtrarReservas(Calendario calendario) {
		Alumno alumno = calendario.getAlumno();

		List<Reserva> reservasAlumno = reservaService.findByAlumno(alumno);

		List<GrupoCalendario> gruposEliminados = new ArrayList<GrupoCalendario>();
		// Eliminamos las reservas anteriores
		for (Reserva r : reservasAlumno) {
			if (r.getEstadoReserva().equals(EstadoReserva.SOLICITUD)) {
				for (ActividadCalendario ac : calendario.getListaActividades()) {
					for (GrupoCalendario gc : ac.getGruposCal()) {
						if (gc.getGrupo().getId().intValue() == (r.getGrupo()
								.getId().intValue())) {
							if (!gc.getGrupo().getEstado()
									.equals(EstadoGrupo.SELECCIONADO)) {
								gruposEliminados.add(gc);
							}
						}
					}
				}
			}
		}

		calendario.setGruposEliminados(gruposEliminados);

		List<GrupoCalendario> gruposNuevos = new ArrayList<GrupoCalendario>();

		// Metemos las nuevas seleccionadas
		for (ActividadCalendario ac : calendario.getListaActividades()) {
			for (GrupoCalendario gc : ac.getGruposCal()) {
				if (gc.getGrupo().getEstado().equals(EstadoGrupo.SELECCIONADO)) {
					boolean reservaCreada = false;
					for (Reserva r : reservasAlumno) {
						if (r.getGrupo().getActividad().getCodigo() == ac
								.getActividad().getCodigo()) {
							reservaCreada = true;
							for (GrupoCalendario gc2 : gruposEliminados) {
								if (gc2.getGrupo().getActividad().getCodigo() == r
										.getGrupo().getActividad().getCodigo()) {
									reservaCreada = false;
								}
							}

						}
					}
					if (!reservaCreada) {
						gruposNuevos.add(gc);
					}
				}
			}
		}
		calendario.setGruposNuevos(gruposNuevos);
	}

	/*
	 * Esta funcion guarda los cambios de manera permanente
	 */
	public void guardarCambios(Calendario calendario) {
		Alumno alumno = calendario.getAlumno();
		List<Reserva> reservasAlumno = reservaService.findByAlumno(alumno);
		// Eliminamos las reservas anteriores
		for (Reserva r : reservasAlumno) {
			if (r.getEstadoReserva().equals(EstadoReserva.SOLICITUD)) {
				for (GrupoCalendario g : calendario.getGruposEliminados()) {
					if (g.getGrupo().getId().intValue() == (r.getGrupo()
							.getId().intValue())) {
						//r.setComentarioCreador(g.getComentario());
						reservaService.delete(r);
					}
				}
			}
		}

		reservasAlumno = reservaService.findByAlumno(alumno);

		// Metemos las nuevas seleccionadas
		for (GrupoCalendario g : calendario.getGruposNuevos()) {
			Reserva reserva = new Reserva();
			HistoricoReserva hr = new HistoricoReserva();
			hr.setAccionReserva(AccionReserva.CREACION);
			hr.setFechaAccion(new Date());
			hr.setUsuario(alumno);
			hr.setComentario(g.getComentario());
			hr.setReserva(reserva);
			reserva.setAlumno(alumno);
			reserva.setEstadoReserva(EstadoReserva.SOLICITUD);
			hr.setEstadoReserva(reserva.getEstadoReserva());
			reserva.setGrupo(grupoService.findById(g.getGrupo().getId()));
			reserva.setPromedio(alumno.getPromedio());
			reserva.addHistoricoReserva(hr);
			reservaService.save(reserva);
		}
		
		//alertService.enviarAlertaResumenReservas(calendario);
	}

}
