package es.ujaen.pfc.reserva.web.alerts;

import java.util.HashMap;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;
import org.springframework.ui.velocity.VelocityEngineUtils;

import es.ujaen.pfc.reserva.model.objects.Alumno;
import es.ujaen.pfc.reserva.model.objects.Reserva;
import es.ujaen.pfc.reserva.web.view.Calendario;

@Component("alertService")
public class AlertServiceImpl implements AlertService {

	@Autowired
	JmsTemplate jmsTemplate;
	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private VelocityEngine velocityEngine;
	

	public AlertServiceImpl() {
		super();
		// TODO Auto-generated constructor stub
	}


	public void enviarAlertaReserva(final Reserva reserva) {
		jmsTemplate.send(new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				return session.createObjectMessage(reserva);
			}
		});

	}

	public void enviarAlertaReservaEmail(Reserva reserva) {
		SimpleMailMessage message = new SimpleMailMessage();

	}

	public void enviarAlertaResumenReservas(Calendario calendario) {
		final Alumno user = calendario.getAlumno();

		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
				message.setTo(user.getEmail());
				message.setFrom("reservas@ujaen.es"); 
				message.setSubject("[Reservas Ujaen] - Resumen de las solicitudes de reserva");
				Map model = new HashMap();
				model.put("user", user);
				String text = VelocityEngineUtils.mergeTemplateIntoString(
						velocityEngine, "emailTemplate.vm", model);
				message.setText(text, true);
			}
		};
		this.mailSender.send(preparator);

	}

}
