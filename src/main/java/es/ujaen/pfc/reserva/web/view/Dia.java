package es.ujaen.pfc.reserva.web.view;

import java.util.Calendar;

public enum Dia {
	LUNES(Calendar.MONDAY), MARTES(Calendar.TUESDAY), MIERCOLES(
			Calendar.WEDNESDAY), JUEVES(Calendar.THURSDAY), VIERNES(
			Calendar.FRIDAY);

	private final int valor;

	Dia(int valor) {
		this.valor = valor;
	}

	public int getValor() {
		return this.valor;
	}
}
