package es.ujaen.pfc.reserva.web;

import java.net.BindException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import es.ujaen.pfc.reserva.model.objects.Alumno;
import es.ujaen.pfc.reserva.model.objects.Reserva;
import es.ujaen.pfc.reserva.model.services.AlumnoService;
import es.ujaen.pfc.reserva.model.services.ReservaService;
import es.ujaen.pfc.reserva.web.view.Calendario;
import es.ujaen.pfc.reserva.web.view.ReservaCalendario;

@Controller
@RequestMapping("/alumnos")
public class AlumnoController {
	private static Log log = LogFactory.getLog(AlumnoController.class);

	@Inject
	private AlumnoService alumnoService;

	@Inject
	private ReservaService reservaService;

	@PreAuthorize("(hasRole('Remote User')")
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Alumno createAlumno(@Valid Alumno alumno, BindingResult result,
			HttpServletResponse response) throws BindException {
		log.debug("Creamos nuevo alumno");
		if (result.hasErrors()) {
			throw new BindException();
		}
		alumnoService.save(alumno);
		log.debug("Alumno creado con id:" + alumno.getIdUsuario());

		response.setHeader("Location", "/alumnos/" + alumno.getIdUsuario());
		return alumno;
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String getAlumno(@PathVariable String id, Model model) {
		log.debug("Obtenemos alumno con id:" + id);
		model.addAttribute("alumno", alumnoService.findById(id));
		return "alumnos/item";
	}

	@PreAuthorize("hasRole('Profesor') or #id == principal.username")
	@RequestMapping(value = "/{id}/reservas", method = RequestMethod.GET)
	public String getReservasAlumno(@PathVariable String id, Model model) {
		log.debug("Obtenemos el listado de reservas del alumno con id:" + id);

		Alumno alumno = alumnoService.findById(id);
		List<Reserva> reservasAlumno = reservaService.findByAlumno(alumno);
		Calendario calendario = new Calendario(alumno, reservasAlumno);
		
		List<ReservaCalendario> reservasAlumnoCal = new ArrayList<ReservaCalendario>();
		for (Reserva r: reservasAlumno){
			reservasAlumnoCal.add(new ReservaCalendario(r));
		}
		
		model.addAttribute("listaReservas", reservasAlumnoCal);
		model.addAttribute("calendario", calendario);

		return "alumnos/reservas";
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteAlumno(@PathVariable String id) {
		log.debug("Eliminamos alumno con id:" + id);
		alumnoService.delete(alumnoService.findById(id));
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateAlumno(@PathVariable String id) {
		log.debug("Actualizamos alumno con id:" + id);
		alumnoService.save(alumnoService.findById(id));
	}

}
