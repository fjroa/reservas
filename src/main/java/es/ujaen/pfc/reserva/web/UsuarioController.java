package es.ujaen.pfc.reserva.web;

import java.net.BindException;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import es.ujaen.pfc.reserva.model.objects.Usuario;
import es.ujaen.pfc.reserva.model.services.UsuarioService;

@Controller
@RequestMapping("/usuarios")
public class UsuarioController {
	private static Log log = LogFactory.getLog(UsuarioController.class);

	@Inject
	private UsuarioService usuarioService;

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Usuario createUsuario(@Valid Usuario usuario, BindingResult result,
			HttpServletResponse response) throws BindException {
		log.debug("Creamos nuevo usuario");
		if (result.hasErrors()) {
			throw new BindException();
		}
		usuarioService.save(usuario);
		log.debug("Usuario creado con id:" + usuario.getIdUsuario());
		
		response.setHeader("Location", "/usuarios/"+usuario.getIdUsuario());
		return usuario;
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String getUsuario(@PathVariable String id, Model model) {
		log.debug("Obtenemos usuario con id:" + id);
		model.addAttribute("usuario", usuarioService.findById(id));
		return "usuarios/item";
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteUsuario(@PathVariable String id) {
		log.debug("Eliminamos usuario con id:" + id);
		usuarioService.delete(usuarioService.findById(id));
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateUsuario(@PathVariable String id) {
		log.debug("Actualizamos usuario con id:" + id);
		usuarioService.save(usuarioService.findById(id));
	}

}
