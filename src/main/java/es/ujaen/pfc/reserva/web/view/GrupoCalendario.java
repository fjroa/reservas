package es.ujaen.pfc.reserva.web.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.ujaen.pfc.reserva.model.objects.EstadoReserva;
import es.ujaen.pfc.reserva.model.objects.Grupo;
import es.ujaen.pfc.reserva.model.objects.SesionLectiva;

public class GrupoCalendario implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3070835058400333051L;
	Grupo grupo;
	Dia dia;
	Date horarioInicio;
	Date horarioFin;
	EstadoReserva estadoReserva;
	List<Integer> codigosGrupo;
	String comentario;
	
	public GrupoCalendario(Grupo grupo) {
		super();
		this.grupo = grupo;
		obtenerHorarioGrupo();
		codigosGrupo = new ArrayList<Integer>();
		estadoReserva = EstadoReserva.SOLICITUD;
	}
	
	public Grupo getGrupo() {
		return grupo;
	}

	public Date getHorarioInicio() {
		return horarioInicio;
	}
	public void setHorarioInicio(Date horarioInicio) {
		this.horarioInicio = horarioInicio;
	}
	public Date getHorarioFin() {
		return horarioFin;
	}
	public void setHorarioFin(Date horarioFin) {
		this.horarioFin = horarioFin;
	}

	public List<Integer> getCodigosGrupo() {
		return codigosGrupo;
	}

	public String getCodigosGrupoCSV(){
		String s = "";
		for (Integer i: codigosGrupo){
			s += "," + i;
		}
		return s;
	}
	public void setCodigosGrupo(List<Integer> codigosGrupo) {
		this.codigosGrupo = codigosGrupo;
	}

	public Dia getDia() {
		return dia;
	}

	public void setDia(Dia dia) {
		this.dia = dia;
	}

	private void obtenerHorarioGrupo() {
		// SIMPLICACION DE ESTA FUNCION, DEBERIA EXTENDERSE PARA CONTAR LOS
		// PERIODOS
		SesionLectiva sesion = grupo.getPeriodos().get(0).getSesiones().get(0);
		this.setDia(sesion.getDia());
		String horaInicio = String.format("%02d", sesion.getHoraInicio()) + ":"
				+ String.format("%02d", sesion.getMinutoInicio());
		this.setHorarioInicio(FechasUtil.convertStringToDate(horaInicio,
				FechasUtil.TYPESDFHOUR));
		String horaFin = String.format("%02d",
				sesion.getHoraInicio() + sesion.getHoraDuracion())
				+ ":"
				+ String.format("%02d",
						sesion.getMinutoInicio() + sesion.getMinutoDuracion());
		this.setHorarioFin(FechasUtil.convertStringToDate(horaFin,
				FechasUtil.TYPESDFHOUR));

	}

	public EstadoReserva getEstadoReserva() {
		return estadoReserva;
	}

	public void setEstadoReserva(EstadoReserva estadoReserva) {
		this.estadoReserva = estadoReserva;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	
	
}
