package es.ujaen.pfc.reserva.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.webflow.action.MultiAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import es.ujaen.pfc.reserva.web.view.Calendario;
import es.ujaen.pfc.reserva.web.view.GrupoCalendario;

@Component
public class EnviarComentariosAction extends MultiAction {

	public Event enviarComentarios(RequestContext context) {

		Calendario calendario = (Calendario) context.getFlowScope().get(
				"calendario");

		List<GrupoCalendario> gruposEliminados = new ArrayList<GrupoCalendario>(
				calendario.getGruposEliminados().size());

		for (GrupoCalendario g : calendario.getGruposEliminados()) {
			g.setComentario(context.getRequestParameters().get(
					"comentario_grupo_" + g.getGrupo().getId()));
			gruposEliminados.add(g);
		}

		calendario.setGruposEliminados(gruposEliminados);

		List<GrupoCalendario> gruposNuevos = new ArrayList<GrupoCalendario>(
				calendario.getGruposNuevos().size());

		for (GrupoCalendario g : calendario.getGruposNuevos()) {
			g.setComentario(context.getRequestParameters().get(
					"comentario_grupo_" + g.getGrupo().getId()));
			gruposNuevos.add(g);
		}

		calendario.setGruposNuevos(gruposNuevos);
		context.getFlowScope().put("calendario", calendario);
		
		return success();
	}
}
