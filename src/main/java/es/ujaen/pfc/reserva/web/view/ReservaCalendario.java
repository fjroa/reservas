package es.ujaen.pfc.reserva.web.view;

import java.io.Serializable;

import es.ujaen.pfc.reserva.model.objects.Reserva;

public class ReservaCalendario implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3070835058400333051L;
	GrupoCalendario grupo;
	Reserva reserva;
	
	
	public ReservaCalendario(Reserva r) {
		super();
		reserva = r;
		grupo=new GrupoCalendario(r.getGrupo());
	}


	public GrupoCalendario getGrupo() {
		return grupo;
	}


	public void setGrupo(GrupoCalendario grupo) {
		this.grupo = grupo;
	}


	public Reserva getReserva() {
		return reserva;
	}


	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}	
	
	
}
