package es.ujaen.pfc.reserva.web;

import java.net.BindException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import es.ujaen.pfc.reserva.model.objects.AccionReserva;
import es.ujaen.pfc.reserva.model.objects.EstadoReserva;
import es.ujaen.pfc.reserva.model.objects.HistoricoReserva;
import es.ujaen.pfc.reserva.model.objects.Reserva;
import es.ujaen.pfc.reserva.model.services.ReservaService;
import es.ujaen.pfc.reserva.model.services.UsuarioService;

@Controller
@RequestMapping("/reservas")
public class ReservasController {
	private static Log log = LogFactory.getLog(ReservasController.class);

	@Inject
	private ReservaService reservaService;
	@Inject
	private UsuarioService usuarioService;
	
	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Reserva createReserva(@Valid Reserva reserva, BindingResult result,
			HttpServletResponse response) throws BindException {
		log.debug("Creamos nueva reserva");
		if (result.hasErrors()) {
			throw new BindException();
		}
		reservaService.save(reserva);
		log.debug("Reserva creada con id:" + reserva.getId());
		
		response.setHeader("Location", "/reservas/"+reserva.getId());
		return reserva;
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String getReserva(@PathVariable long id, Model model) {
		log.debug("Obtenemos reserva con id:" + id);
		model.addAttribute("reserva", reservaService.findById(id));
		return "reservas/item";
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteReserva(@PathVariable long id) {
		log.debug("Eliminamos reserva con id:" + id);
		reservaService.delete(reservaService.findById(id));
	}

	@PreAuthorize("hasRole('Remote User')")
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateReserva(@PathVariable long id) {
		log.debug("Actualizamos reserva con id:" + id);
		reservaService.save(reservaService.findById(id));
	}
	
	@PreAuthorize("hasRole('Profesor') and #id = principal.username")
	@RequestMapping(value = "/{id}/preasignar", method = RequestMethod.GET)
	public String preasignarReserva(@PathVariable long id, Principal principal) {
		Reserva r =  reservaService.findById(id);
		HistoricoReserva hr = new HistoricoReserva();
		hr.setAccionReserva(AccionReserva.PREASIGNACION);
		hr.setFechaAccion(new Date());
		hr.setUsuario(usuarioService.findById(principal.getName()));
		r.setEstadoReserva(EstadoReserva.PREASIGNADA);
		hr.setEstadoReserva(r.getEstadoReserva());
		hr.setReserva(r);
		r.addHistoricoReserva(hr);
		reservaService.saveReservaPreasignada(r);
		return "redirect:/grupos/"+r.getGrupo().getId()+"/info";
	}
	
	@PreAuthorize("hasRole('Profesor') and #id = principal.username")
	@RequestMapping(value = "/{id}/desasignar", method = RequestMethod.GET)
	public String desasignarReserva(@PathVariable long id, Principal principal) {
		Reserva r =  reservaService.findById(id);
		HistoricoReserva hr = new HistoricoReserva();
		hr.setAccionReserva(AccionReserva.DENEGACION);
		hr.setFechaAccion(new Date());
		hr.setUsuario(usuarioService.findById(principal.getName()));
		hr.setReserva(r);
		r.setEstadoReserva(EstadoReserva.SOLICITUD);
		hr.setEstadoReserva(r.getEstadoReserva());
		r.addHistoricoReserva(hr);
		reservaService.deleteReservaPreasignada(r);
		return "redirect:/grupos/"+r.getGrupo().getId()+"/info";
	}
	
	@RequestMapping(value = "/{id}/report", method = RequestMethod.GET)
	public String generarDataSourceReport(@PathVariable Long id, Model model) {
		Reserva reserva = reservaService.findById(id);
		Collection<Reserva> col = new ArrayList<Reserva>();
		col.add(reserva);
		model.addAttribute("reservaData", col);
		model.addAttribute("SubReportData", (Collection<HistoricoReserva>) reserva.getHistoricosReserva());
		return ("reservaReport");
	}
}
