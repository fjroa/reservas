package es.ujaen.pfc.reserva.web.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import es.ujaen.pfc.reserva.model.objects.Actividad;
import es.ujaen.pfc.reserva.model.objects.Alumno;
import es.ujaen.pfc.reserva.model.objects.EstadoGrupo;
import es.ujaen.pfc.reserva.model.objects.EstadoReserva;
import es.ujaen.pfc.reserva.model.objects.Grupo;
import es.ujaen.pfc.reserva.model.objects.Periodo;
import es.ujaen.pfc.reserva.model.objects.Reserva;
import es.ujaen.pfc.reserva.model.objects.SesionLectiva;
import es.ujaen.pfc.reserva.model.services.GrupoService;

public class Calendario implements Serializable {

	public class ItemCalendario implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -4925748746484869966L;
		protected String clase;
		protected String texto;
		protected int tipoEvento;
		protected int posicionEnEvento;

		public ItemCalendario() {
			super();
		}

		public ItemCalendario(String clase, String texto) {
			super();
			this.clase = clase;
			this.texto = texto;
		}

		public String getClase() {
			return clase;
		}

		public void setClase(String clase) {
			this.clase = clase;
		}

		public String getTexto() {
			return texto;
		}

		public void setTexto(String texto) {
			this.texto = texto;
		}

		public int getTipoEvento() {
			return tipoEvento;
		}

		public void setTipoEvento(int tipoEvento) {
			this.tipoEvento = tipoEvento;
		}

		public int getPosicionEnEvento() {
			return posicionEnEvento;
		}

		public void setPosicionEnEvento(int posicionEnEvento) {
			this.posicionEnEvento = posicionEnEvento;
		}

	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -4077356523013265580L;
	private static final int MINUTOS_POR_HORA = 60;
	static final private int NUMERO_DE_HORAS = 14;
	static final private int HORA_INICIAL = 8;
	static final private int ITEMS_POR_HORA = 2;
	static final private int DIAS_POR_SEMANA = 5;

	static final private int POS_COMPLETO = 0;
	static final private int POS_INICIO = 1;
	static final private int POS_CENTRO = 2;
	static final private int POS_FIN = 3;
	static final private int POS_VACIO_INICIO = 4;
	static final private int POS_VACIO_CENTRO = 5;

	static final private int TIPO_RESERVA = 1;
	static final private int TIPO_CONFLICTO = 2;
	static final private int TIPO_ALERTA = 3;
	static final private int TIPO_TEORIA = 4;
	static final private int TIPO_VACIO = 0;

	private ItemCalendario[][] items;

	private List<ActividadCalendario> listaActividades;
	private Alumno alumno;
	private List<GrupoCalendario> gruposEliminados;
	private List<GrupoCalendario> gruposNuevos;

	public Calendario() {
		super();
		init();
	}

	public Calendario(Alumno alumno, List<Reserva> reservasAlumno) {
		super();
		init();

		listaActividades = new ArrayList<ActividadCalendario>();

		for (Actividad a : alumno.getActividades()) {
			ActividadCalendario ac = new ActividadCalendario(a);
			for (Grupo g : a.getGrupos()) {
				if (!g.getEstado().equals(EstadoGrupo.TEORIA)) {
					GrupoCalendario gc = new GrupoCalendario(g);
					gc.setCodigosGrupo(extractCodigos(g));
					ac.addGrupo(gc);
				}
			}
			listaActividades.add(ac);
		}

		for (Reserva r : reservasAlumno) {
			this.addGrupo(r.getGrupo(), r.getEstadoReserva());
			if(!r.getEstadoReserva().equals(EstadoReserva.SOLICITUD)) {
				this.actualizarGrupoAsignado(r.getGrupo());
			}
		}

		for (Grupo g : alumno.getGruposTeoria()) {
			this.addGrupoVisualizar(g);
		}

		this.alumno = alumno;
	}

	private void init() {
		// Inicializamos el calendario
		items = new ItemCalendario[DIAS_POR_SEMANA + 1][NUMERO_DE_HORAS
				* ITEMS_POR_HORA];
		// Cargamos las horas y los dias vacios
		for (int dia = 0; dia < DIAS_POR_SEMANA + 1; dia++) {
			for (int hora = 0; hora < NUMERO_DE_HORAS; hora++) {
				for (int j = 0; j < ITEMS_POR_HORA; j++) {
					int minutos = (MINUTOS_POR_HORA / ITEMS_POR_HORA) * j;
					ItemCalendario itemHora = new ItemCalendario();
					if (j % ITEMS_POR_HORA == 0) {
						if (dia == 0) {
							itemHora.setClase("hora05");
							itemHora.setTexto(String.format("%02d", hora
									+ HORA_INICIAL)
									+ ":" + String.format("%02d", minutos));
						} else {
							itemHora.setClase("cal05");
						}
					} else {
						if (dia == 0) {
							itemHora.setClase("hora04");
							itemHora.setTexto(String.format("%02d", hora
									+ HORA_INICIAL)
									+ ":" + String.format("%02d", minutos));
						} else {
							itemHora.setClase("cal04");
						}
					}
					items[dia][hora * ITEMS_POR_HORA + j] = itemHora;
				}
			}
		}
	}

	public ItemCalendario[][] getItems() {
		return items;
	}

	public void setItems(ItemCalendario[][] items) {
		this.items = items;
	}

	public void addGrupoVisualizar(Grupo grupo) {
		for (Periodo p : grupo.getPeriodos()) {
			for (SesionLectiva s : p.getSesiones()) {
				int celdaDia = s.getDia().getValor() - 1;
				int celdaInicio = (s.getHoraInicio() - HORA_INICIAL)
						* ITEMS_POR_HORA;
				int posInicioMinutos = s.getMinutoInicio()
						/ (MINUTOS_POR_HORA / ITEMS_POR_HORA);
				if (posInicioMinutos > 0) {
					celdaInicio += ITEMS_POR_HORA - posInicioMinutos;
				}
				int numCeldas = (s.getHoraDuracion() * ITEMS_POR_HORA)
						+ s.getMinutoDuracion()
						/ (MINUTOS_POR_HORA / ITEMS_POR_HORA);
				for (int i = 0; i < numCeldas; i++) {
					modificarCelda(grupo, p, celdaDia, celdaInicio, i,
							posInicioMinutos, numCeldas);
				}
			}
		}
	}

	public void addGrupo(Grupo grupo, EstadoReserva estadoReserva) {
		addGrupoVisualizar(grupo);
		grupo.setEstado(EstadoGrupo.SELECCIONADO);
		GrupoCalendario gc = new GrupoCalendario(grupo);
		gc.setEstadoReserva(estadoReserva);
		gc.setCodigosGrupo(extractCodigos(grupo));
		for (int i = 0; i < listaActividades.size(); i++) {
			ActividadCalendario ac = listaActividades.get(i);
			if (ac.getActividad().getCodigo() == grupo.getActividad()
					.getCodigo()) {
				for (int j = 0; j < ac.getGruposCal().size(); j++) {
					GrupoCalendario gcaux = ac.getGruposCal().get(j);
					if (gcaux.getGrupo().getId().intValue() == grupo.getId()
							.intValue()) {
						ac.getGruposCal().set(j, gc);
						break;
					}
				}
				listaActividades.set(i, ac);
				break;
			}
		}

	}

	public void deleteGrupo(Grupo grupo) {

		init();

		for (Grupo g : alumno.getGruposTeoria()) {
			this.addGrupoVisualizar(g);
		}

		for (int i = 0; i < listaActividades.size(); i++) {
			ActividadCalendario ac = listaActividades.get(i);
			if (ac.getActividad().getCodigo() == grupo.getActividad()
					.getCodigo()) {
				for (int j = 0; j < ac.getGruposCal().size(); j++) {
					GrupoCalendario gcaux = ac.getGruposCal().get(j);
					if (gcaux.getGrupo().getId().intValue() == grupo.getId()
							.intValue()) {
						gcaux.setCodigosGrupo(extractCodigos(grupo));
						gcaux.getGrupo().setEstado(grupo.getEstado());
						ac.getGruposCal().set(j, gcaux);
						break;
					}
				}
				listaActividades.set(i, ac);
				break;
			}
		}

		for (ActividadCalendario ac : listaActividades) {
			for (GrupoCalendario gc : ac.getGruposCal()) {
				if (gc.getGrupo().getEstado().equals(EstadoGrupo.SELECCIONADO)) {
					addGrupoVisualizar(gc.getGrupo());
				}
			}
		}
	}

	private void modificarCelda(Grupo grupo, Periodo p, int celdaDia,
			int celdaInicio, int i, int posInicioMinutos, int numCeldas) {
		ItemCalendario item = evaluarCelda(grupo, p, celdaDia, celdaInicio, i,
				posInicioMinutos, numCeldas);
		String claseNueva = "cal" + item.tipoEvento + item.posicionEnEvento;
		item.setClase(claseNueva);
		items[celdaDia][celdaInicio + i] = item;
	}

	private ItemCalendario evaluarCelda(Grupo grupo, Periodo p, int celdaDia,
			int celdaInicio, int i, int posInicioMinutos, int numCeldas) {
		ItemCalendario item = new ItemCalendario();
		ItemCalendario celdaOriginal = items[celdaDia][celdaInicio + i];
		item.posicionEnEvento = POS_CENTRO;
		item.tipoEvento = TIPO_RESERVA;
		if (i == 0) {
			item.setTexto(grupo.getActividad().getAbreviatura());
			if (numCeldas == 1) {
				item.posicionEnEvento = POS_COMPLETO;
			} else {
				item.posicionEnEvento = POS_INICIO;
			}
		} else {
			item.setTexto(celdaOriginal.getTexto());
		}
		if (i == numCeldas - 1) {
			item.posicionEnEvento = POS_FIN;
		}

		Integer tipoOriginal = Integer.parseInt(celdaOriginal.getClase()
				.substring(3, 4));

		if (tipoOriginal != TIPO_VACIO) {
			item.tipoEvento = TIPO_CONFLICTO;
		} else if (grupo.getEstado().equals(EstadoGrupo.TEORIA)) {
			item.tipoEvento = TIPO_TEORIA;

		}
		return item;
	}

	private List<Integer> extractCodigos(Grupo grupo) {
		List<Integer> lista = new ArrayList<Integer>();
		for (Periodo p : grupo.getPeriodos()) {
			for (SesionLectiva s : p.getSesiones()) {
				int celdaDia = s.getDia().getValor() - 1;
				int celdaInicio = (s.getHoraInicio() - HORA_INICIAL)
						* ITEMS_POR_HORA;
				int posInicioMinutos = s.getMinutoInicio()
						/ (MINUTOS_POR_HORA / ITEMS_POR_HORA);
				if (posInicioMinutos > 0) {
					celdaInicio += ITEMS_POR_HORA - posInicioMinutos;
				}
				int numCeldas = (s.getHoraDuracion() * ITEMS_POR_HORA)
						+ s.getMinutoDuracion()
						/ (MINUTOS_POR_HORA / ITEMS_POR_HORA);
				for (int i = 0; i < numCeldas; i++) {
					ItemCalendario item = evaluarCelda(grupo, p, celdaDia,
							celdaInicio, i, posInicioMinutos, numCeldas);
					lista.add(celdaDia * 10000 + item.tipoEvento * 1000
							+ item.posicionEnEvento * 100 + celdaInicio + i);
				}
			}
		}
		return lista;
	}

	/*
	 * Este m�todo cambia el estado del grupo de modo que la visualizacion sepa
	 * el estado del mismo, teniendo en cuenta el horario particular de cada
	 * alumno
	 */

	public void actualizarGrupo(Grupo grupo) {
		GrupoCalendario gc = null;
		ActividadCalendario actividadCalendario = null;
		for (ActividadCalendario ac : listaActividades) {
			for (GrupoCalendario gcaux : ac.getGruposCal()) {
				if (gcaux.getGrupo().getId().intValue() == grupo.getId()
						.intValue()) {
					gc = gcaux;
					actividadCalendario = ac;
				}
			}
		}

		// Determinamos si el alumno ya ha hecho una reserva en la misma
		// actividad
		if (!gc.getGrupo().getEstado().equals(EstadoGrupo.CERRADO)
				&& !gc.getGrupo().getEstado().equals(EstadoGrupo.TEORIA)) {
			boolean reservaEnActividad = false;
			for (ActividadCalendario ac : listaActividades) {
				if (ac.getActividad().getCodigo() == gc.getGrupo()
						.getActividad().getCodigo()) {
					for (GrupoCalendario g : ac.getGruposCal()) {
						if (g.getGrupo().getEstado()
								.equals(EstadoGrupo.SELECCIONADO)) {
							// En caso afirmativo comprobamos si el grupo de la
							// reserva
							// es
							// el mismo o no
							reservaEnActividad = true;
							if (g.getGrupo().equals(gc.getGrupo())) {
								for (int i = 0; i < gc.getCodigosGrupo().size(); i++) {
									int codigo = gc.getCodigosGrupo().get(i);
									int dia = codigo / 10000;
									codigo %= 1000;
									codigo %= 100;
									int codigoNuevo = dia * 10000 + codigo;
									if (codigo % ITEMS_POR_HORA == 0) {
										codigoNuevo += 500;
									} else {
										codigoNuevo += 400;
									}
									gc.getCodigosGrupo().set(i, codigoNuevo);
								}
							} else {
								gc.getGrupo().setEstado(
										EstadoGrupo.NO_SELECCIONABLE);
							}
							break;
						}
					}
				}
			}
			// En caso contrario comprobamos si el grupo ha dado algun conflicto
			if (!reservaEnActividad) {
				if (isGrupoEnConflicto(gc)) {
					gc.getGrupo().setEstado(EstadoGrupo.CONFLICTO);
					// TODO EL GRUPO SE MUESTRA COMO CONFLICTO
					for (int i = 0; i < gc.getCodigosGrupo().size(); i++) {
						int code = gc.getCodigosGrupo().get(i);
						int dia = code / 10000;
						code %= 1000;
						int codigoNuevo = dia * 10000 + TIPO_CONFLICTO * 1000
								+ code;
						gc.getCodigosGrupo().set(i, codigoNuevo);
					}
				} else {
					gc.setCodigosGrupo(extractCodigos(grupo));
					gc.getGrupo().setEstado(grupo.getEstado());
				}
			}
		}

		for (int i = 0; i < listaActividades.size(); i++) {
			ActividadCalendario ac = listaActividades.get(i);
			if (ac.getActividad().getCodigo() == actividadCalendario
					.getActividad().getCodigo()) {
				for (int j = 0; j < ac.getGruposCal().size(); j++) {
					GrupoCalendario gcaux = ac.getGruposCal().get(j);
					if (gcaux.getGrupo().getId().intValue() == gc.getGrupo()
							.getId().intValue()) {
						ac.getGruposCal().set(j, gc);
						break;
					}
				}
				listaActividades.set(i, ac);
				break;
			}
		}
	}

	private boolean isGrupoEnConflicto(GrupoCalendario gc) {
		for (ActividadCalendario ac : listaActividades) {
			for (GrupoCalendario g : ac.getGruposCal()) {
				if (g.getGrupo().getEstado().equals(EstadoGrupo.SELECCIONADO)) {
					if (g.getGrupo().isEnConflicto(gc.getGrupo())) {
						return true;
					}
				}
			}
		}
		return false;

	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public List<ActividadCalendario> getListaActividades() {
		return listaActividades;
	}

	public void setListaActividades(List<ActividadCalendario> listaActividades) {
		this.listaActividades = listaActividades;
	}

	public List<GrupoCalendario> getGruposEliminados() {
		return gruposEliminados;
	}

	public void setGruposEliminados(List<GrupoCalendario> gruposEliminados) {
		this.gruposEliminados = gruposEliminados;
	}

	public List<GrupoCalendario> getGruposNuevos() {
		return gruposNuevos;
	}

	public void setGruposNuevos(List<GrupoCalendario> gruposNuevos) {
		this.gruposNuevos = gruposNuevos;
	}

	public void actualizarGrupoAsignado(Grupo grupo) {
		for (Periodo p : grupo.getPeriodos()) {
			for (SesionLectiva s : p.getSesiones()) {
				int celdaDia = s.getDia().getValor() - 1;
				int celdaInicio = (s.getHoraInicio() - HORA_INICIAL)
						* ITEMS_POR_HORA;
				int posInicioMinutos = s.getMinutoInicio()
						/ (MINUTOS_POR_HORA / ITEMS_POR_HORA);
				if (posInicioMinutos > 0) {
					celdaInicio += ITEMS_POR_HORA - posInicioMinutos;
				}
				int numCeldas = (s.getHoraDuracion() * ITEMS_POR_HORA)
						+ s.getMinutoDuracion()
						/ (MINUTOS_POR_HORA / ITEMS_POR_HORA);

				ItemCalendario item = items[celdaDia][celdaInicio + numCeldas-1];
				item.setClase(item.getClase() + " SELECCIONADO");
				items[celdaDia][celdaInicio + numCeldas-1] = item;
			}
		}
	}
}
