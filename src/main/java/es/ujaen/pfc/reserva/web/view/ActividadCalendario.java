package es.ujaen.pfc.reserva.web.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.ujaen.pfc.reserva.model.objects.Actividad;

public class ActividadCalendario implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3070835058400333051L;
	List<GrupoCalendario> gruposCal;
	Actividad actividad;
	
	
	public ActividadCalendario(Actividad a) {
		super();
		actividad = a;
		gruposCal = new ArrayList<GrupoCalendario>();
	}
	

	public List<GrupoCalendario> getGruposCal() {
		return gruposCal;
	}


	public void setGruposCal(List<GrupoCalendario> gruposCal) {
		this.gruposCal = gruposCal;
	}


	public Actividad getActividad() {
		return actividad;
	}

	public void addGrupo(GrupoCalendario gc) {
		gruposCal.add(gc);
	}


	public void removeGrupo(GrupoCalendario gc) {
		gruposCal.remove(gc);
	}



	
	
	

	
	
}
