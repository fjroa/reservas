package es.ujaen.pfc.reserva.web.alerts;

import java.util.List;

import es.ujaen.pfc.reserva.model.objects.Reserva;
import es.ujaen.pfc.reserva.web.view.Calendario;
import es.ujaen.pfc.reserva.web.view.GrupoCalendario;

public interface AlertService {
	void enviarAlertaReserva(Reserva r);

	void enviarAlertaResumenReservas(Calendario calendario);
}
