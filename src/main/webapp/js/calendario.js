	var backupElements = new Array();
	var backupDia= new Array();
	var backupHora= new Array();
	
	jQuery(document).ready(function(){
		
		var $scrollingDiv = $("#calendario");
 		var $asignaturasDiv = $("#asignaturas");
		var $headerDiv = $("#header");
		
		$(".asignatura").find("li:.NO_SELECCIONABLE").hide(function(){
			$(this).parent().parent().find("span:.expand").html("+");
		});
		$(".asignatura").find("li:.MAS_INFO").hide(function(){
			$(this).parent().parent().find("span:.expand").html("+");
		});

		
		$(".asignatura").find("li:.CERRADO").hide(function(){
			$(this).parent().parent().find("span:.expand").html("+");
		});
		
		$('.asignatura > h3').toggle(function() {
			$(this).parent().find("li:.NO_SELECCIONABLE").toggle('fast');
			$(this).parent().find("li:.MAS_INFO").toggle('fast');
			$(this).parent().find("li:.CERRADO").toggle('fast');
			$(this).find("span:.expand").html("-");
		}, function() {
			$(this).parent().find("li:.NO_SELECCIONABLE").toggle('fast');
			$(this).parent().find("li:.CERRADO").toggle('fast');
			$(this).parent().find("li:.MAS_INFO").toggle('fast');
			$(this).find("span:.expand").html("+");
		});
		
		$(window).scroll(function(){			
			$scrollingDiv
				.stop()
				if ($asignaturasDiv.height() - $scrollingDiv.height() > 0) {
					if (($(window).scrollTop()) > $headerDiv.height() + 75){
						if ( ($(window).scrollTop()) + $headerDiv.height() + 75 < $asignaturasDiv.height()){
							$scrollingDiv.animate({"marginTop": $(window).scrollTop() - $headerDiv.height() - 75 + "px"}, "slow" );			
						} else {
							$scrollingDiv.animate({"marginTop": $asignaturasDiv.height()  - ($headerDiv.height() + 75 )*2 + "px"}, "slow" );			
						}
					} else {
						$scrollingDiv.animate({"marginTop": 10 + "px"}, "slow" );
					}
				}
		});
	});

	function mostrarEnCalendario() {
		/*
			Esta funcion muestra en el calendario los grupos cuando se pasa el raton encima de un grupo cualquiera
			Para interpretar los valores que acepta la funcion se detalla su entrada
    			El primer argumento es el nombre de la asignatura
				A continuacion se indican los valores que se mostraran en el formulario en cada casilla de manera individual siguiendo los siguientes criterios 
				
				Decenas de millar - Dia de la semana: Lunes 1, Martes 2, Miercoles 3, Jueves 4, Viernes 5
				Millares - Tipo de evento: Reserva 1,Conflicto 2, Alerta 3, cal 0
				Centenas - Posicion de evento: Completo 0, Inicio 1, Centro 2, Fin 3, Par 4, Impar 5
				Decenas y Unidades - La hora del evento, esta hora se calcula de la forma 2x-15 (para las 19h seria 38-15=23), 
						    		 si los minutos son :30 entonces se le debe sumar uno a esta cifra
				
				Ejemplo: Un evento que mostraria una reserva un martes de 12:00 a 13:30 tendr�a los siguientes valores: "Nombre", 21109, 21210, 21311
		*/
		
		var nombreAsignatura = arguments[0];
		for (var i = 1; i < arguments.length; i++) {
			var number = arguments[i];
			//extraemos el dia
			var diaVar = parseInt((number/10000));
			//extraemos el tipo de evento
			var tmp = number%10000;
			var tipoEvento = parseInt(tmp/1000);
			//extraemos la posicion
			tmp = tmp%1000;
			var posEvento = parseInt(tmp/100);
			//extraemos la hora
			var hora = parseInt(tmp %100) +1;
			
			//Buscamos en la lista correspondiente para modificar su valor 
			var ul = document.getElementById('dia'+diaVar);
			var items = ul.getElementsByTagName("li");
			
			//guardamos una copia para limpiarla con la funcion backupCalendario
			backupElements[i-1] = items[hora].cloneNode(true);
			backupDia[i-1] = diaVar;
			backupHora[i-1] = hora;
			
			//Cambiamos el elemento
			items[hora].className = 'cal'+tipoEvento+posEvento;
			if(posEvento <= 1) {
				items[hora].innerHTML = nombreAsignatura;
			}
			if(tipoEvento == 0) items[hora].innerHTML = '';
	  }
	}
	
	function backupCalendario() {
		//Esta funcion restablece el estado del calendario despues de usar la funcion mostrarEnCalendario
		//iteramos por la lista de backups
		for (var i = 0, len = backupElements.length; i < len; i++ ) {
			//Cambiamos el elemento
			var ul = document.getElementById('dia'+backupDia[i]);
			var items = ul.getElementsByTagName("li");
			items[backupHora[i]].className = backupElements[i].className;
			items[backupHora[i]].innerHTML = backupElements[i].innerHTML;
		}
		//Limpiamos los arrays
		backupElements.length = 0;
		backupDia.length = 0;
		backupHora.length = 0;
	}