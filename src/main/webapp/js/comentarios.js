jQuery(document)
		.ready(
				function() {
					$(".asignatura").find("span:.COMENTARIO").hide(
							function() {
								$(this).parent()
										.find("span:.expand_comentario").html(
												"A�adir comentario");
								});
					
					$("li:.ALUMNOS_DISPONIBLES").hide(function(){
						$(this).parent().find("span:.expand").html("(+) Expandir Lista");
					});
					
					$("span:.expand")
					.toggle(
							function() {
								$("li:.ALUMNOS_DISPONIBLES").toggle(
										'fast');
								$(this).html("(-) Contraer Lista");
							}, function() {
							$("li:.ALUMNOS_DISPONIBLES").toggle(
							'fast');
					$(this).html("(+) Expandir Lista");
				});
							
					$(".asignatura")
							.find("span:.COMENTARIO")
							.parent()
							.find("span:.expand_comentario")
							.toggle(
									function() {
										$(this).parent().find(
												"span:.COMENTARIO").toggle(
												'fast');
										$(this).html("Eliminar Comentario");
									},
									function() {
										$(this)
												.parent()
												.find("span:.COMENTARIO")
												.toggle(
														function() {
															$(this)
																	.parent()
																	.find(
																			"textarea")
																	.val("");
															$(this)
																	.parent()
																	.find(
																			"p:.charsRemaining")
																	.html(
																			"N�mero M�ximo de Car�cteres : 255");
														});
										$(this).html("A�adir comentario");
									});

					var onEditCallback = function(remaining) {
						$(this).siblings('.charsRemaining').text(
								"Quedan " + remaining + " de 255 caracteres");

						if (remaining > 0) {
							$(this).css('background-color', 'white');
						}
					}

					var onLimitCallback = function() {
						$(this).css('background-color', 'red');
					}

					$('textarea[maxlength]').limitMaxlength({
						onEdit : onEditCallback,
						onLimit : onLimitCallback
					});
				});
// -->

jQuery.fn.limitMaxlength = function(options) {

	var settings = jQuery.extend({
		attribute : "maxlength",
		onLimit : function() {
		},
		onEdit : function() {
		}
	}, options);

	// Event handler to limit the textarea
	var onEdit = function() {
		var textarea = jQuery(this);
		var maxlength = parseInt(textarea.attr(settings.attribute));

		if (textarea.val().length > maxlength) {
			textarea.val(textarea.val().substr(0, maxlength));

			// Call the onlimit handler within the scope of the textarea
			jQuery.proxy(settings.onLimit, this)();
		}

		// Call the onEdit handler within the scope of the textarea
		jQuery.proxy(settings.onEdit, this)(maxlength - textarea.val().length);
	}

	this.each(onEdit);

	return this.keyup(onEdit).keydown(onEdit).focus(onEdit).live('input paste',
			onEdit);
}