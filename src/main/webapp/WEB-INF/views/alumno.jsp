<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<h2>
	<spring:message code='menu.reservaPracticas' />
</h2>
				
<h4>
	<a href="<c:url value="/reservasAlumno" />">Solicitud de reservas</a>
</h4>
<security:authentication
				property="principal.username" var="userID"/>
<h4>
	<a href="<c:url value="/alumnos/${userID}/reservas"/>">Información de las reservas
		solicitadas</a>
</h4>