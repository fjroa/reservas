<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link href="<c:url value="/css/calendario.css"/>" rel="stylesheet"
	type="text/css" media="screen, print" />

<h2>Actividad ${actividad.codigo}</h2>
<div id="detalle">
	<dl>
		<dt>Codigo</dt>
		<dd>${actividad.codigo}</dd>
		<dt>Nombre</dt>
		<dd>${actividad.nombre}</dd>
		<dt>Abreviatura</dt>
		<dd>${actividad.abreviatura}</dd>
		<dt>Coordinador</dt>
		<dd><a href="<c:url value="/usuarios/${actividad.coordinador.idUsuario}" />">${actividad.coordinador.idUsuario}</a></dd>
		<dt>Grupos</dt>
		<dd />
		<c:forEach var="grupo" items="${actividad.grupos}">
			<dt />
			<dd>
				<a href="<c:url value="/grupos/${grupo.id}"/>">${grupo.id}</a>
			</dd>
		</c:forEach>
	</dl>
</div>