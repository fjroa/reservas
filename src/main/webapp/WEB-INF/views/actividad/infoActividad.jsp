<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<link href="<c:url value="/css/calendario.css"/>" rel="stylesheet"
	type="text/css" media="screen, print" />
<c:if test="${!empty infoApp}">
	<div class="info_app">
		<s:message code="actividad.abreviaturaActualizada" />
	</div>
</c:if>
<h2>${actividad.nombre}</h2>
<h3>Información de la Asignatura</h3>
<div class="detalle">
	<dl>
		<dt>Codigo</dt>
		<dd>${actividad.codigo}</dd>
		<dt>Nombre</dt>
		<dd>${actividad.nombre}</dd>
		<dt>Abreviatura</dt>
		<dd>${actividad.abreviatura}</dd>
		<dt>Coordinador</dt>
		<dd>${actividad.coordinador.nombre}</dd>
	</dl>
</div>
<sec:authorize access="hasRole('Profesor')">
	<h3>Editar abreviatura de la actividad</h3>
	<div class="submit" style="text-align: left">
		<form method="POST" action="actualizarAbreviatura">
			<c:if test="${!empty errorForm}">
				<div class="error_app">
					<s:message code="actividad.errorForm" />
				</div>
			</c:if>
			<label for="abreviatura">Nueva abreviatura:</label> <input
				id="abreviatura" name="abreviatura" width="50px"
				value="${actividad.abreviatura}" /> <br />
			<br />
			<br /> <input class="submit" type="submit"
				value="Actualizar Abreviatura" />
		</form>
	</div>
</sec:authorize>