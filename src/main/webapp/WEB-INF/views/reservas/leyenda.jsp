<%@ page contentType="text/html" pageEncoding="ISO-8859-15"
	language="java"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<h2 class="skip">
	<spring:message code='menu.menuLocal' />
</h2>
<ul class="localNav" style="">

	<li><a href="/srv/es/informacionacademica/asignaturasmatriculadas"><spring:message
				code='menu.asignaturasMatriculadas'  /></a></li>
	<li><a href="/srv/es/informacionacademica/expediente"><spring:message
				code='menu.expedienteAcademico'  /></a></li>
	<li><a href="/srv/es/informacionacademica/catalogoguiasdocentes"><spring:message
				code='menu.guiasDocentes'  /></a></li>
	<li class="active"><a href="/reservas/"><spring:message
				code='menu.reservaPracticas'  /></a></li>

	<div id="leyenda">
		<h3>Informaci�n</h3>
	</div>
	<div id="texto_leyenda" class="bluetable2">
		<p>Pase por encima de cada grupo para visualizar en el calendario
			de la derecha como quedar�a configurado su horario.</p>
		</p>
		Existe un c�digo de colores para diferenciar el estado de cada grupo:
		</p>
		<dl>
			<dt style="background-color: #2850A0"></dt>
			<dd>
				<strong>Grupo Disponible</strong>: Puede solicitar la reserva en
				dicho grupo
			</dd>
			<br />
			<dt style="background-color: #005000"></dt>
			<dd>
				<strong>Grupo Seleccionado</strong>: Tiene una solicitud de reserva
				creada para este grupo
			</dd>
			<br />
			<dt style="background-color: #500000"></dt>
			<dd>
				<strong>Grupo Lleno</strong>: El grupo no tiene plazas disponibles
			</dd>
			<br />
			<dt style="background-color: #A02800"></dt>
			<dd>
				<strong>Grupo en Conflicto</strong>: El horario del grupo coincide
				con el de otro grupo seleccionado previamente
			</dd>
			<br />
			<dt style="background-color: #666666"></dt>
			<dd>
				<strong>Grupo No Disponible</strong>: No puede crear una solicitud
				de reserva para este grupo
			</dd>
			<br />
			<dt style="background-color: #A33EFF"></dt>
			<dd>
				<strong>Grupo de Teor�a</strong>: No pertenece a la parte pr�ctica
			</dd>
			<br />

		</dl>
	</div>
</ul>