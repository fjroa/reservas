<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link href="<c:url value="/css/calendario.css"/>" rel="stylesheet"
	type="text/css" media="screen, print" />

<h2>Reserva ${reserva.id}</h2>
<div id="detalle">
	<dl>
		<dt>ID</dt>
		<dd>${reserva.id}</dd>
		<dt>Alumno</dt>
		<dd><a href="<c:url value="/alumnos/${reserva.alumno.idUsuario}"/>">${reserva.alumno.idUsuario}</a></dd>
		<dt>Promedio</dt>
		<dd>${reserva.promedio}</dd>
		<dt>Grupo</dt>
		<dd><a href="<c:url value="/grupos/${reserva.grupo.id}"/>">${reserva.grupo.id}</a></dd>
		<dt>Creador</dt>
		<dd><a href="<c:url value="/usuarios/${reserva.creador.idUsuario}"/>">${reserva.creador.idUsuario}</a></dd>
		<dt>Fecha Creaci�n</dt>
		<dd>
			<fmt:formatDate value="${reserva.fechaCreacion}"
				pattern="dd/MM/yyyy - hh:mm:ss" />
		</dd>
		<dt>Estado Reserva</dt>
		<dd>${reserva.estadoReserva}</dd>
	</dl>
</div>