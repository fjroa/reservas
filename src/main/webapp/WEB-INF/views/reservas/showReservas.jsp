<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link href="<c:url value="/css/calendario.css"/>" rel="stylesheet"
	type="text/css" media="screen, print" />
<!-- Javascript -->
<script type="text/javascript" src="<c:url value="/js/calendario.js"/> " /></script>

<h2>
	<s:message code='alumno.informacionReservas.titulo' />
</h2>

<div id="asignaturas">
	<h2>Listado de Asignaturas</h2>
	<ul class="lista_asignaturas">
		<c:forEach var="actividad" items="${calendario.listaActividades}">
			<li class="asignatura">
				<h3>
					${actividad.actividad.nombre} (${actividad.actividad.abreviatura})
					<span class="expand"></span>
				</h3>
				<ul class="grupos">
					<c:forEach var="gc" items="${actividad.gruposCal}"
						varStatus="grupoCounter">

						<c:set var="function"
							value="onmouseout='backupCalendario()' onmouseover=\" mostrarEnCalendario('${actividad.actividad.abreviatura}'${gc.codigosGrupoCSV})\"" />
						<c:choose>
							<c:when test="${gc.grupo.estado == 'SELECCIONADO'}">
								<c:set var="eventId" value="deleteGrupo" />
								<c:set var="linkFunction" value="${function}" />
								<c:set var="linkText" value="Eliminar Seleccion" />
								<c:set var="function" value="" />
							</c:when>
							<c:otherwise>
								<c:set var="eventId" value="addGrupo" />
								<c:set var="linkText" value="Seleccionar" />
								<c:set var="linkFunction" value="" />
							</c:otherwise>
						</c:choose>

						<li class="${gc.grupo.estado}"
							<c:if test="${(gc.grupo.estado != 'CERRADO') and (gc.grupo.estado != 'NO_SELECCIONABLE')}">
								${function}
							</c:if>>
							<h4 class="detalle_asignatura">
								<dl>
									<dt>Horario :</dt>
									<c:forEach var="periodo" items="${gc.grupo.periodos}"
										varStatus="status">
										${not status.first ? '<dt></dt>' : ''}
										<dd>
											Del
											<fmt:formatDate value="${periodo.fechaInicio}"
												pattern="dd/MM/yyyy" />
											al
											<fmt:formatDate value="${periodo.fechaFin}"
												pattern="dd/MM/yyyy" />
										</dd>
										<c:forEach var="sesion" items="${periodo.sesiones}">
											<dt></dt>
											<dd>
												<s:message code="calendario.dia{${sesion.dia.valor}}" />
												de
												<fmt:formatNumber minIntegerDigits="2"
													value="${sesion.horaInicio}" />:<fmt:formatNumber minIntegerDigits="2"
													value="${sesion.minutoInicio}" />
												a
												<fmt:formatNumber minIntegerDigits="2"
													value="${sesion.horaInicio+sesion.horaDuracion}" />:<fmt:formatNumber minIntegerDigits="2"
													value="${sesion.minutoInicio+sesion.minutoDuracion}" />
											</dd>
										</c:forEach>
									</c:forEach>
									<dt>Imparte :</dt>
									<c:forEach var="profesor" items="${gc.grupo.profesores}"
										varStatus="status">
										${not status.first ? '<dt></dt>' : ''}
										<dd>${profesor.nombre}</dd>
									</c:forEach>
									<dt>Plazas :</dt>
									<dd>${gc.grupo.numeroPlazasOcupadas}/${gc.grupo.numeroPlazas}
									</dd>
									<dt>Nota de Corte :</dt>
									<dd>${gc.grupo.notaCorte}</dd>
									<dt>Reservas Solicitadas :</dt>
									<dd>${gc.grupo.numeroReservasSolicitadas}</dd>
									<dt>Nota de Corte de las Reservas :</dt>
									<dd>${gc.grupo.notaCorteReservas}</dd>
									<c:choose>
										<c:when test="${(gc.grupo.estado == 'CERRADO')}">
											<span class="infoGrupo">Grupo Cerrado</span>
										</c:when>
										<c:when test="${(gc.grupo.estado == 'NO_SELECCIONABLE')}">
											<span class="infoGrupo">Grupo No Seleccionable</span>
										</c:when>
										<c:when test="${(gc.estadoReserva != 'SOLICITUD')}">
											<span class="infoGrupo">RESERVA ${gc.estadoReserva}</span>
										</c:when>
										<c:otherwise>
											<a
												href="${flowExecutionUrl}&_eventId=${eventId}&idGrupo=${gc.grupo.id}"${linkFunction}">${linkText}</a>
										</c:otherwise>
									</c:choose>
								</dl>
							</h4>
						</li>
					</c:forEach>
				</ul>
			</li>
		</c:forEach>
	</ul>

</div>
<div id="calendario">
	<h2>
		<s:message code='calendario.titulo' />
	</h2>
	<c:forEach var="dia" items="${calendario.items}" varStatus="diaCounter">
		<ul id="dia${diaCounter.index}">
			<li class="dia"><span><s:message
						code='calendario.dia{${diaCounter.index}}' /></span></li>
			<c:forEach var="item" items="${dia}">
				<li class="${item.clase}">${item.texto}</li>
			</c:forEach>
		</ul>
	</c:forEach>
</div>
<div class="submit">
	<sf:form>
		<input type="hidden" name="_flowExecutionKey"
			value="${flowExecutionKey}" />
		<input type="submit" name="_eventId_mostrarResultados"
			value="Finalizar" />
		<input type="submit" name="_eventId_cancel" value="Cancelar" />

	</sf:form>
</div>