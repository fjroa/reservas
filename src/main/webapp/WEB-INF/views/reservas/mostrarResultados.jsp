<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>

<link href="<c:url value="/css/calendario.css"/>" rel="stylesheet"
	type="text/css" media="screen, print" />
<!-- Javascript -->
<script type="text/javascript" src="<c:url value="/js/comentarios.js"/> " /></script>


<h2>Resultado de Solicitud de las Reservas</h2>
<sf:form>
	<div id="asignaturas" style="width: 100%">
		<c:if test="${empty calendario.gruposEliminados}">
			<h3>No se solicit� eliminar ninguna reserva existente</h3>
		</c:if>
		<c:if test="${not empty calendario.gruposEliminados}">
			<h3>Se eliminar�n las siguientes reservas</h3>
			<ul class="lista_asignaturas">
				<c:forEach var="gc" items="${calendario.gruposEliminados}">
					<li class="asignatura">
						<h3>${gc.grupo.actividad.nombre}</h3>
						<ul class="grupos">
							<li class="NO_SELECCIONABLE">
								<h4 class="detalle_asignatura">
								<dl>
									<dt>Horario :</dt>
									<c:forEach var="periodo" items="${gc.grupo.periodos}"
										varStatus="status">
										${not status.first ? '<dt></dt>' : ''}
										<dd>
											Del
											<fmt:formatDate value="${periodo.fechaInicio}"
												pattern="dd/MM/yyyy" />
											al
											<fmt:formatDate value="${periodo.fechaFin}"
												pattern="dd/MM/yyyy" />
										</dd>
										<c:forEach var="sesion" items="${periodo.sesiones}">
											<dt></dt>
											<dd>
												<s:message code="calendario.dia{${sesion.dia.valor}}" />
												de
												<fmt:formatNumber minIntegerDigits="2"
													value="${sesion.horaInicio}" />:<fmt:formatNumber minIntegerDigits="2"
													value="${sesion.minutoInicio}" />
												a
												<fmt:formatNumber minIntegerDigits="2"
													value="${sesion.horaInicio+sesion.horaDuracion}" />:<fmt:formatNumber minIntegerDigits="2"
													value="${sesion.minutoInicio+sesion.minutoDuracion}" />
											</dd>
										</c:forEach>
									</c:forEach>
									<dt>Imparte :</dt>
									<c:forEach var="profesor" items="${gc.grupo.profesores}"
										varStatus="status">
										${not status.first ? '<dt></dt>' : ''}
										<dd>${profesor.nombre}</dd>
									</c:forEach>
									<dt>Plazas :</dt>
									<dd>${gc.grupo.numeroPlazasOcupadas}/${gc.grupo.numeroPlazas}
									</dd>
									<dt>Nota de Corte :</dt>
									<dd>${gc.grupo.notaCorte}</dd>
									<dt>Reservas Solicitadas :</dt>
									<dd>${gc.grupo.numeroReservasSolicitadas}</dd>
									<dt>Nota de Corte de las Reservas :</dt>
									<dd>${gc.grupo.notaCorteReservas}</dd> <span
										class="expand_comentario"></span> <br /> <span
										class="COMENTARIO"> Comentario para los responsables
										del grupo de pr�cticas: <br />
										<p class="charsRemaining">N�mero M�ximo de Car�cteres :
											255</p> <textarea rows="5" maxlength="255"
											name="comentario_grupo_${gc.grupo.id}"></textarea>
									</span>
								</dl>
								</h4>
							</li>
						</ul>
					</li>

				</c:forEach>
			</ul>

		</c:if>

		<c:if test="${empty calendario.gruposNuevos}">
			<h3>No se solicit� a�adir ninguna nueva reserva</h3>
		</c:if>
		<c:if test="${not empty calendario.gruposNuevos}">
			<h3>Se a�adir�n las siguientes reservas</h3>
			<ul class="lista_asignaturas">
				<c:forEach var="gc" items="${calendario.gruposNuevos}">
					<li class="asignatura">
						<h3>${gc.grupo.actividad.nombre}</h3>
						<ul class="grupos">
							<li class="${gc.grupo.estado}">
								<h4 class="detalle_asignatura">
								<dl>
									<dt>Horario :</dt>
									<c:forEach var="periodo" items="${gc.grupo.periodos}"
										varStatus="status">
										${not status.first ? '<dt></dt>' : ''}
										<dd>
											Del
											<fmt:formatDate value="${periodo.fechaInicio}"
												pattern="dd/MM/yyyy" />
											al
											<fmt:formatDate value="${periodo.fechaFin}"
												pattern="dd/MM/yyyy" />
										</dd>
										<c:forEach var="sesion" items="${periodo.sesiones}">
											<dt></dt>
											<dd>
												<s:message code="calendario.dia{${sesion.dia.valor}}" />
												de
												<fmt:formatNumber minIntegerDigits="2"
													value="${sesion.horaInicio}" />:<fmt:formatNumber minIntegerDigits="2"
													value="${sesion.minutoInicio}" />
												a
												<fmt:formatNumber minIntegerDigits="2"
													value="${sesion.horaInicio+sesion.horaDuracion}" />:<fmt:formatNumber minIntegerDigits="2"
													value="${sesion.minutoInicio+sesion.minutoDuracion}" />
											</dd>
										</c:forEach>
									</c:forEach>
									<dt>Imparte :</dt>
									<c:forEach var="profesor" items="${gc.grupo.profesores}"
										varStatus="status">
										${not status.first ? '<dt></dt>' : ''}
										<dd>${profesor.nombre}</dd>
									</c:forEach>
									<dt>Plazas :</dt>
									<dd>${gc.grupo.numeroPlazasOcupadas}/${gc.grupo.numeroPlazas}
									</dd>
									<dt>Nota de Corte :</dt>
									<dd>${gc.grupo.notaCorte}</dd>
									<dt>Reservas Solicitadas :</dt>
									<dd>${gc.grupo.numeroReservasSolicitadas}</dd>
									<dt>Nota de Corte de las Reservas :</dt>
									<dd>${gc.grupo.notaCorteReservas}</dd><span
										class="expand_comentario"></span> <br /> <span
										class="COMENTARIO"> Comentario para los responsables
										del grupo de pr�cticas: <br />
										<p class="charsRemaining">N�mero M�ximo de Car�cteres :
											255</p> <textarea rows="5" maxlength="255"
											name="comentario_grupo_${gc.grupo.id}"></textarea>
									</span>
								</h4>
							</li>
						</ul>
					</li>
				</c:forEach>
			</ul>
		</c:if>
	</div>
	<div class="submit">
		<input type="hidden" name="_flowExecutionKey"
			value="${flowExecutionKey}" /> <input type="submit"
			name="_eventId_confirmar" value="Confirmar" /> <input type="submit"
			name="_eventId_volver" value="Volver" /><input type="submit"
			name="_eventId_cancel" value="Cancelar" />
	</div>
</sf:form>
