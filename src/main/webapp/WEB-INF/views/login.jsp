<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>

<div class="siduja_logo" id="login">
	<img src="<c:url value="/images3/Logo4.png"/>" width="510" height="120" />
</div>
<div class="siduja_tapa">
	<form 
		action='<c:url value="/j_spring_security_check"></c:url>'
		method="POST">
		<p align="left">
			<label for="username" style="width: 200px; text-align: center;">Nombre
				de usuario<br /> <input type="text" name="j_username" id="username"
				class="input" value="" size="20">
			</label> <br /> <br />
		</p>
		<p>
			<label for="password" style="width: 200px; text-align: center;">Clave
				de acceso<br /> <input type="password" name="j_password"
				id="password" value="" size="20">
			</label> <br />
		</p>
		<br>
		<p class="submit">
			<input type="submit"
				onclick="document.getElementById('conectando').style.visibility='visible';document.getElementById('wp-submit').style.visibility='hidden';submit();"
				name="wp-submit" id="wp-submit" value="Login &raquo;" /> <input
				type="hidden" name="RelayState" value="" />
		</p>
		<div id="conectando" style="visibility: hidden; font-size: large;">
			<br />CONECTANDO<img src="<c:url value="/images3/conectando.gif"/>"
				height="12">
		</div>
	</form>
</div>