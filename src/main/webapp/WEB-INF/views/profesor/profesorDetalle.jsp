<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link href="<c:url value="/css/calendario.css"/>" rel="stylesheet"
	type="text/css" media="screen, print" />

<h2>Profesor ${profesor.idUsuario}</h2>
<div id="detalle">
	<dl>
		<dt>ID</dt>
		<dd>${profesor.idUsuario}</dd>
		<dt>Nombre</dt>
		<dd>${profesor.nombre}</dd>
		<dt>Actividades</dt>
		<dd/>
		<c:forEach var="actividad" items="${profesor.actividades}">
			<dt/><dd><a href="<c:url value="/actividades/${actividad.codigo}"/>">${actividad.codigo}</a></dd>
		</c:forEach>
		<dt>Grupos</dt>
		<dd/>
		<c:forEach var="grupo" items="${profesor.grupos}">
			<dt/><dd><a href="<c:url value="/grupos/${grupo.id}"/>">${grupo.id}</a></dd>
		</c:forEach>
	</dl>
</div>