<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<h4>
<security:authentication
				property="principal.username" var="userID"/>
	<a href="<c:url value="/profesores/${userID}/grupos"/>">Información de
		los grupos</a>
</h4>