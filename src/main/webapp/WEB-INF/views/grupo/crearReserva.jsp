<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<link href="<c:url value="/css/calendario.css"/>" rel="stylesheet"
	type="text/css" media="screen, print" />

<script type="text/javascript"
	src="<c:url value="/js/comentarios.js"/> " /></script>

<h2>Crear nueva reserva</h2>

<h3>Informaci�n del Grupo</h3>
<div class="detalle">
	<dl>
		<dt>Profesor</dt>
		<dd>
			<c:forEach var="profesor" items="${grupo.profesores}">
				${profesor.idUsuario} &nbsp;
		</c:forEach>
		</dd>
		<dt>Estado Grupo</dt>
		<dd>${grupo.estado}</dd>
		<dt>Horario</dt>
		<dd>
			<c:forEach var="periodo" items="${grupo.periodos}">
					Del <fmt:formatDate value="${periodo.fechaInicio}"
					pattern="dd/MM/yyyy" /> al <fmt:formatDate
					value="${periodo.fechaFin}" pattern="dd/MM/yyyy" />

				<c:forEach var="sesion" items="${periodo.sesiones}"> - 
						<s:message code="calendario.dia{${sesion.dia.valor}}" /> de: <fmt:formatNumber
						minIntegerDigits="2" value="${sesion.horaInicio}" />:<fmt:formatNumber
						minIntegerDigits="2" value="${sesion.minutoInicio}" /> a: <fmt:formatNumber
						minIntegerDigits="2"
						value="${sesion.horaInicio+sesion.horaDuracion}" />:<fmt:formatNumber
						minIntegerDigits="2"
						value="${sesion.minutoInicio+sesion.minutoDuracion}" />
				</c:forEach>
			</c:forEach>
		</dd>
		<dt>Numero de plazas</dt>
		<dd>${grupo.numeroPlazasOcupadas}/${grupo.numeroPlazas}</dd>
		<dt>Nota de Corte del Grupo</dt>
		<dd>${grupo.notaCorte}</dd>
		<dt>Nota de Corte de las Reservas</dt>
		<dd>${grupo.notaCorteReservas}</dd>
		<dt>Reservas Solicitadas</dt>
		<dd>${grupo.numeroReservasSolicitadas}</dd>
		<dt>Nota de Corte de las Reservas</dt>
		<dd>${grupo.notaCorteReservas}</dd>
	</dl>
</div>
<h3>
	Alumnos candidatos - ${fn:length(alumnosDisponibles)} <span class="expand"></span>
</h3>
<li class="ALUMNOS_DISPONIBLES" style="list-style: none">
	<ul class="detalle">
		<c:choose>
			<c:when test="${empty alumnosDisponibles}">
				<h4>No hay alumnos candidatos para este grupo.</h4>
			</c:when>
			<c:otherwise>
				<c:forEach var="alumno" items="${alumnosDisponibles}">
					<li class="reserva">
						<dl>
							<dt>Alumno</dt>
							<dd>${alumno.nombre}</dd>
							<dt>Promedio</dt>
							<dd>${alumno.promedio}</dd>
							<a href='<s:url value="/alumnos/${alumno.idUsuario}/reservas"/>'>Ver
								Reservas</a>
						</dl>
					</li>

				</c:forEach>
			</c:otherwise>
		</c:choose>
	</ul>
</li>
<c:if test="${!empty alumnosDisponibles}">
	<h3>Seleccione el alumno para crear la reserva</h3>
	<div class="submit" style="text-align: left">
		<form method="POST" action="crearReserva">
			<select name="alumnoReserva">
				<c:forEach items="${alumnosDisponibles}" var="alumno">
					<option value="${alumno.idUsuario}">Alumno:
						${alumno.nombre} - Promedio: ${alumno.promedio}</option>
				</c:forEach>
			</select> <p></p>
			<li class="asignatura" style="list-style: none"><span class="expand_comentario"  style="float: left; color: #049FE1"></span> <br />
				<span class="COMENTARIO"> Comentario para el alumno: <br />
					<p class="charsRemaining">N�mero M�ximo de Car�cteres : 255</p> <textarea
						rows="5" maxlength="255" name="comentarioCreador"></textarea>
			</span></li> <br /> <input type="submit" value="Crear Reserva" />
		</form>
	</div>
</c:if>