<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link href="<c:url value="/css/calendario.css"/>" rel="stylesheet"
	type="text/css" media="screen, print" />

<h2>Grupo ${grupo.id}</h2>
<div id="detalle">
	<dl>
		<dt>ID</dt>
		<dd>${grupo.id}</dd>
		<dt>Actividad</dt>
		<dd>
			<a href="<c:url value="/actividades/${grupo.actividad.codigo}"/>">${grupo.actividad.codigo}</a>
		</dd>
		<dt>Profesores</dt>
		<dd />
		<c:forEach var="profesor" items="${grupo.profesores}">
			<dt />
			<dd>
				<a href="<c:url value="/profesores/${profesor.idUsuario}"/>">${profesor.idUsuario}</a>
			</dd>
		</c:forEach>
		<dt>Periodos</dt>
		<dd>
		<ul>
			<c:forEach var="periodo" items="${grupo.periodos}">
				<li>Periodo ${periodo.id}</li>
				<li>Fecha Inicio: <fmt:formatDate value="${periodo.fechaInicio}"
						pattern="dd/MM/yyyy - hh:mm:ss" />
				</li>
				<li>Fecha Fin: <fmt:formatDate value="${periodo.fechaFin}"
						pattern="dd/MM/yyyy - hh:mm:ss" />
				</li>
				<li>Sesiones</li>
					<ul>
					<c:forEach var="sesion" items="${periodo.sesiones}">
						<li>Sesion ${sesion.id}</li>
						<li>Dia: <s:message code="calendario.dia{${sesion.dia.valor}}" /> </li>
						<li>Hora Inicio: <fmt:formatNumber minIntegerDigits="2" value="${sesion.horaInicio}"/> : <fmt:formatNumber minIntegerDigits="2" value="${sesion.minutoInicio}"/></li>
						<li>Duracion: <fmt:formatNumber minIntegerDigits="2" value="${sesion.horaDuracion}"/>h <fmt:formatNumber minIntegerDigits="2" value="${sesion.minutoDuracion}"/>min</li>
					</c:forEach>
				</li>
			</c:forEach>
			</ul>
		</dd>
		<dt>Numero de plazas ocupadas</dt>
		<dd>${grupo.numeroPlazasOcupadas}</dd>
		<dt>Numero de plazas</dt>
		<dd>${grupo.numeroPlazas}</dd>
		<dt>Nota de Corte del Grupo</dt>
		<dd>${grupo.notaCorte}</dd>
		<dt>Estado Grupo</dt>
		<dd>${grupo.estado}</dd>
	</dl>
</div>