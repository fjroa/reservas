<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<link href="<c:url value="/css/calendario.css"/>" rel="stylesheet"
	type="text/css" media="screen, print" />
<c:if test="${!empty infoApp}">
	<div class="info_app">
		<s:message code="grupo.numeroPlazasActualizadas" />
	</div>
</c:if>
<h2>Grupo ${grupo.id} de ${grupo.actividad.nombre}</h2>
<h3>Informaci�n del Grupo</h3>
<div class="detalle">
	<dl>
		<dt>Profesor</dt>
		<dd>
			<c:forEach var="profesor" items="${grupo.profesores}">
				${profesor.idUsuario} &nbsp;
		</c:forEach>
		</dd>
		<dt>Estado Grupo</dt>
		<dd>${grupo.estado}</dd>
		<dt>Horario</dt>
		<dd>
			<c:forEach var="periodo" items="${grupo.periodos}">
					Del <fmt:formatDate value="${periodo.fechaInicio}"
					pattern="dd/MM/yyyy" /> al <fmt:formatDate
					value="${periodo.fechaFin}" pattern="dd/MM/yyyy" />

				<c:forEach var="sesion" items="${periodo.sesiones}"> - 
						<s:message code="calendario.dia{${sesion.dia.valor}}" /> de: <fmt:formatNumber
						minIntegerDigits="2" value="${sesion.horaInicio}" />:<fmt:formatNumber
						minIntegerDigits="2" value="${sesion.minutoInicio}" /> a: <fmt:formatNumber
						minIntegerDigits="2"
						value="${sesion.horaInicio+sesion.horaDuracion}" />:<fmt:formatNumber
						minIntegerDigits="2"
						value="${sesion.minutoInicio+sesion.minutoDuracion}" />
				</c:forEach>
			</c:forEach>
		</dd>
		<dt>Numero de plazas</dt>
		<dd>${grupo.numeroPlazas}</dd>
		<dt>Nota de Corte del Grupo</dt>
		<dd>${grupo.notaCorte}</dd>
		<dt>Nota de Corte de las Reservas</dt>
		<dd>${grupo.notaCorteReservas}</dd>
	</dl>
</div>
<sec:authorize access="hasRole('Profesor')">
	<h3>Plazas asignadas - ${grupo.numeroPlazasOcupadas} de
		${grupo.numeroPlazas}</h3>
	<ul class="detalle">
		<c:choose>
			<c:when test="${empty plazasAsignadas}">
				<h4>No existen plazas asignadas en el grupo.</h4>
			</c:when>
			<c:otherwise>
				<c:forEach var="reserva" items="${plazasAsignadas}">
					<li class="reserva">
						<dl>
							<dt>Alumno</dt>
							<dd>
								<a
									href='<s:url value="/alumnos/${reserva.alumno.idUsuario}/reservas"/>'>${reserva.alumno.nombre}</a>
							</dd>
							<dt>Promedio</dt>
							<dd>${reserva.promedio}</dd>
							<c:if test="${reserva.estadoReserva == 'PREASIGNADA'}">
								<a href='<s:url value="/reservas/${reserva.id}/desasignar"/>'>Denegar
									Plaza</a>
							</c:if>

						</dl>
					</li>

				</c:forEach>
			</c:otherwise>
		</c:choose>
	</ul>
	<h3>Reservas solicitadas - ${grupo.numeroReservasSolicitadas}</h3>
	<ul class="detalle">
		<c:choose>
			<c:when test="${empty reservasSolicitadas}">
				<h4>No existen reservas solicitadas en el grupo.</h4>
			</c:when>
			<c:otherwise>
				<c:forEach var="reserva" items="${reservasSolicitadas}">
					<li class="reserva">
						<dl>
							<dt>Alumno</dt>
							<dd>
								<a
									href='<s:url value="/alumnos/${reserva.alumno.idUsuario}/reservas"/>'>${reserva.alumno.nombre}</a>
							</dd>
							<dt>Promedio</dt>
							<dd>${reserva.promedio}</dd>
							<c:if test="${grupo.numeroPlazasOcupadas<grupo.numeroPlazas}">
								<a href='<s:url value="/reservas/${reserva.id}/preasignar"/>'>Preasignar
									Plaza</a>
							</c:if>
						</dl>
					</li>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</ul>
	<div class="submit" style="text-align: left">
		<c:url value="/grupos/${grupo.id}/crearReserva" var="url" />
		<sf:form action="${url}" method="get">
			<input type="submit" value="Crear Reserva" />
		</sf:form>
	</div>
</sec:authorize>
<sec:authorize access="hasRole('Departamento')">
	<h3>Editar n�mero de plazas del grupo</h3>
	<div class="submit" style="text-align: left">
		<form method="POST" action="actualizarPlazas">
			<c:if test="${!empty errorForm}">
				<div class="error_app">
					<s:message code="grupo.errorForm" />
					: <strong>${grupo.numeroPlazasOcupadas} </strong>
				</div>
			</c:if>
			<label for="intNumber">Nuevo numero de plazas:</label> <input
				id="intNumber" name="numeroPlazas" type="number"
				min="${grupo.numeroPlazasOcupadas}" width="20px" value="${grupo.numeroPlazas}"
				required="true" /> <br /><br /><br /> <input class="submit" type="submit"
				value="Actualizar Plazas" />
		</form>
	</div>
</sec:authorize>