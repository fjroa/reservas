<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>

<%@ page contentType="text/html" pageEncoding="UTF-8" language="java"%>

<a href="#content" class="skip"
	title="<spring:message code='cabecera.saltar.title'/>"><spring:message
		code='cabecera.saltar.text' /></a>
<ul class="langNav">
	<li id="spanish"><a href="?lang=es"><spring:message
				code='cabecera.lang.spa' /></a></li>
	<li id="english"><a href="?lang=en"><spring:message
				code='cabecera.lang.eng' /></a></li>
	<li id="pdf"></li>
</ul>
<h1>
	<a href="http://www.ujaen.es/" accesscode="h"><span><spring:message
				code='universidad.nombre' /></span></a>
</h1>
<p class="slogan">
	<spring:message code='cabecera.slogan' />
</p>

<h2 class="skip">
	<spring:message code='menu.menuPrincipal' />
</h2>
<ul class="mainNav">
	<li><a href="<c:url value="/"/>"> <span id="pageHome">&nbsp;</span>
	</a></li>
	<li><a href="/srv/es/informacionacademica" accesscode="1"> <spring:message
				code='menu.serviciosAcademicos' /></a></li>
	<li><a href="/srv/es/informaciongeneral" accesscode="2"> <spring:message
				code='menu.informacionGeneral' /></a></li>
	<li><a href="/srv/es/operaciones" accesscode="3"> <spring:message
				code='menu.operaciones' /></a></li>
</ul>
<h2 class="skip">
	<spring:message code='cabecera.informacionUsuario' />
</h2>
<ul class="secondaryNav">
	<li><span style="background: #FF0000; color: #FFFFFF">${nombreUsuario}</span></li>
	<li class="intranet"><a
		href="<c:url value="/j_spring_security_logout"/>" accesscode="i"><spring:message
				code='cabecera.informacionUsuario.salir' /></a></li>
</ul>
