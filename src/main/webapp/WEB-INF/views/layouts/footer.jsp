<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<h2>
	<spring:message code='universidad.nombre'  />
</h2>

<p>
	<spring:message code='pie.direccion'  />
	<br />
	<spring:message code='pie.soporte'  />
	<a href="mailto:gestion@ujaen.es">gestion@ujaen.es</a> <br /> <a
		href="http://www10.ujaen.es/aviso-legal"><spring:message
			code='pie.avisoLegal'  /></a> | <a
		href="http://administracionelectronica.ujaen.es/node/129"><spring:message
			code='pie.sugerencias'  /></a>
</p>
<h2 class="skip">
	<spring:message code='menu.menuPrincipal'  />
</h2>
<ul class="mainNav">
	<li><a href="index.html"><span id="pageHome">&nbsp;</span></a></li>
	<li><a href="/srv/es/informacionacademica" accesscode="1"> <spring:message
				code='menu.serviciosAcademicos'  /></a></li>
	<li><a href="/srv/es/informaciongeneral" accesscode="2"> <spring:message
				code='menu.informacionGeneral'  /></a></li>
	<li><a href="/srv/es/operaciones" accesscode="3"> <spring:message
				code='menu.operaciones'  /></a></li>
</ul>
