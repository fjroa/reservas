<%@ page contentType="text/html" pageEncoding="ISO-8859-15"
	language="java"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<h2 class="skip">
	<spring:message code='menu.menuLocal'  />
</h2>
<ul class="localNav" style="">
	<li><a href="/srv/es/informacionacademica/asignaturasmatriculadas"><spring:message
				code='menu.asignaturasMatriculadas'  /></a></li>
	<li><a href="/srv/es/informacionacademica/expediente"><spring:message
				code='menu.expedienteAcademico'  /></a></li>
	<li><a href="/srv/es/informacionacademica/catalogoguiasdocentes"><spring:message
				code='menu.guiasDocentes'  /></a></li>
	<li class="active"><a href='<spring:url value="/"/>'><spring:message
				code='menu.reservaPracticas'  /></a></li>
</ul>
