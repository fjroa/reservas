<%@ page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<ul class="breadcrumb">
	<li><a href="<c:url value="/"/>"><spring:message code='menu.inicio'
				 /> &gt;</a></li>
	<li><a href="/srv/es/informacionacademica"><spring:message
				code='menu.serviciosAcademicos'  /></a></li>
</ul>