<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>

<%@ page contentType="text/html" pageEncoding="UTF-8" language="java"%>

<a href="#content" class="skip"
	title="<spring:message code='cabecera.saltar.title'/>"><spring:message
		code='cabecera.saltar.text' /></a>
<ul class="langNav">
	<li id="spanish"><a href="?lang=es"><spring:message
				code='cabecera.lang.spa' /></a></li>
	<li id="english"><a href="?lang=en"><spring:message
				code='cabecera.lang.eng' /></a></li>
	<li id="pdf"></li>
</ul>
<h1>
	<a href="http://www.ujaen.es/" accesscode="h"><span><spring:message
				code='universidad.nombre' /></span></a>
</h1>
<p class="slogan">
	<spring:message code='cabecera.slogan' />
</p>

<h2 class="skip">
	<spring:message code='menu.menuPrincipal' />
</h2>
<ul class="mainNav">
	<li><a href="#" class="active"><span id="pageHome">&nbsp;</span></a></li>
	<li><a href="http://www.ujaen.es/home/informacion.html"
		accesskey="1">Con&oacute;cenos</a></li>
	<li><a href="http://www.ujaen.es/home/acceso.html" accesskey="2">Acceso
			/ Admisi&oacute;n </a></li>
	<li><a href="http://www.ujaen.es/home/iacademica.html"
		accesskey="3">Informaci&oacute;n Acad&eacute;mica</a></li>
	<li><a href="http://www.ujaen.es/home/investigacion.html"
		accesskey="4">Investigaci&oacute;n</a></li>
	<li><a href="http://www.ujaen.es/home/participacion.html"
		accesskey="5">Participaci&oacute;n y Colaboraci&oacute;n </a></li>
</ul>
<form id="search" action="http://busqueda.ujaen.es/search" method="get">
	<p>
		<input type="hidden" value="0" name="entqr" /> <input type="hidden"
			value="1" name="ud" /> <input type="hidden" value="date:D:L:d1"
			name="sort" /> <input type="hidden" value="xml_no_dtd" name="output" />
		<input type="hidden" value="UTF-8" name="oe" /><input type="hidden"
			value="ISO-8859-1" name="ie" /> <input type="hidden" value="web2010"
			name="client" /> <input type="hidden" value="web2010"
			name="proxystylesheet" /> <input type="hidden"
			value="default_collection" name="site" /> <label for="searchString">Buscar:</label>
		<input type="text" value="" id="searchString" name="q" /> <input
			type="image"
			src="/autenticacion/module.php/themeujaen2/resources/btsearch.png"
			name="btnG" id="submit" value="Buscar" alt="Buscar" />
	</p>
</form>
<h2 class="skip">
	<spring:message code='cabecera.informacionUsuario' />
</h2>
<h2 class="skip">Men&uacute; global</h2>
<ul class="secondaryNav">
	<li><a href="http://www.ujaen.es/home/mapa.html" accesskey="m">Mapa</a></li>
	<li><a
		href="http://www.ujaen.es/sci/redes/telefonia/fija/rainfo.html"
		accesskey="c">Contacto</a></li>
	<li><a href="http://www.ujaen.es/home/accesibilidad.html"
		accesskey="a">Accesibilidad</a></li>
	<li class="intranet"><a
		href="https://www.ujaen.es/home/intranet.html" accesskey="i">Intranet</a></li>
</ul>
