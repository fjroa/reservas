<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xhtml="true">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<link rel="icon" href="<c:url value="/favicon2.ico"/>" />
<link rel="shortcut icon" href="<c:url value="/favicon2.ico"/>"/>
<meta name="author" content="fjroa" />

<link href="<c:url value="/css/ujaen3.css"/>" rel="stylesheet" type="text/css"
	media="screen, print" />
<link href="<c:url value="/css/ujaencv.css"/>" rel="stylesheet" type="text/css"
	media="screen, print" />
<!--[if lte IE 8]>
	<link href="<c:url value="/css/ujaen3ie.css"/>" rel="stylesheet" type="text/css" media="screen, print" />
	<![endif]-->
<!--[if IE 6]>
	<link href="<c:url value="/css/ujaen3ie6.css"/>" rel="stylesheet" type="text/css" media="screen, print" />
	<![endif]-->

<!-- Javascript -->
<script type="text/javascript" src="<c:url value="/js/jquery/jquery-1.6.2.js"/> "/></script>
<title><tiles:insertAttribute name="title" ignore="true" /></title>
</head>

<body id="micrositeA">
	<div id="wrapper">
		<div id="header">
			<tiles:insertAttribute name="header" />
		</div>
		<div id="content">
			<tiles:insertAttribute name="breadcrumb" />
			<div id="mainContent">
				<tiles:insertAttribute name="body" />
				<tiles:insertAttribute name="menu" />
			</div>
		</div>
		<div id="footer">
			<tiles:insertAttribute name="footer" />
		</div>
	</div>
</body>
</html>



