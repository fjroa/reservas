<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- Javascript -->
<script type="text/javascript">
	jQuery(document).ready(function(){
	
	$(".asignatura").find("li").hide(function(){
	$(this).parent().parent().find("span:.expand").html("+"); });
	
	$('.asignatura > h3').toggle(function() {
	$(this).parent().find("li").toggle('fast');
	$(this).find("span:.expand").html("-"); }, function() {
	$(this).parent().find("li").toggle('fast');
	$(this).find("span:.expand").html("+"); }); });
</script>

<link href="<c:url value="/css/calendario.css"/>" rel="stylesheet"
	type="text/css" media="screen, print" />

<h2>
	<s:message code='alumno.informacionReservas.titulo' />
</h2>

<div id="asignaturas" style="width: 100%">
	<h2>Listado de Asignaturas</h2>
	<ul class="asignaturas">
		<c:forEach var="actividad" items="${actividades}">
			<li class="asignatura">
				<h3>
					${actividad.actividad.nombre} (${actividad.actividad.abreviatura})
					<span class="expand"></span>
				</h3>
				<ul class="grupos">
					<c:forEach var="gc" items="${actividad.gruposCal}">

						<li class="${gc.grupo.estado}">

							<h4 class="detalle_asignatura">
								<dl>
									<dt>Horario :</dt>
									<c:forEach var="periodo" items="${gc.grupo.periodos}"
										varStatus="status">
										${not status.first ? '<dt></dt>' : ''}
										<dd>
											Del
											<fmt:formatDate value="${periodo.fechaInicio}"
												pattern="dd/MM/yyyy" />
											al
											<fmt:formatDate value="${periodo.fechaFin}"
												pattern="dd/MM/yyyy" />
										</dd>
										<c:forEach var="sesion" items="${periodo.sesiones}">
											<dt></dt>
											<dd>
												<s:message code="calendario.dia{${sesion.dia.valor}}" />
												de
												<fmt:formatNumber minIntegerDigits="2"
													value="${sesion.horaInicio}" />:<fmt:formatNumber minIntegerDigits="2"
													value="${sesion.minutoInicio}" />
												a
												<fmt:formatNumber minIntegerDigits="2"
													value="${sesion.horaInicio+sesion.horaDuracion}" />:<fmt:formatNumber minIntegerDigits="2"
													value="${sesion.minutoInicio+sesion.minutoDuracion}" />
											</dd>
										</c:forEach>
									</c:forEach>
									<dt>Imparte :</dt>
									<c:forEach var="profesor" items="${gc.grupo.profesores}"
										varStatus="status">
										${not status.first ? '<dt></dt>' : ''}
										<dd>${profesor.nombre}</dd>
									</c:forEach>
									<dt>Plazas :</dt>
									<dd>${gc.grupo.numeroPlazasOcupadas}/${gc.grupo.numeroPlazas}
									</dd>
									<dt>Reservas Solicitadas :</dt>
									<dd>${gc.grupo.numeroReservasSolicitadas}</dd>
								<a href='
								<s:url value="/grupos/${gc.grupo.id}/info" />
								'>Gestionar Grupo</a>
								</dl>
							</h4>
						</li>
					</c:forEach>
				</ul>
			</li>
		</c:forEach>
	</ul>

</div>
