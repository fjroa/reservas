<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link href="<c:url value="/css/calendario.css"/>" rel="stylesheet"
	type="text/css" media="screen, print" />

<h2>Departamento ${departamento.idUsuario}</h2>
<div id="detalle">
	<dl>
		<dt>ID</dt>
		<dd>${departamento.idUsuario}</dd>
		<dt>Nombre</dt>
		<dd>${departamento.nombre}</dd>
		<dt>Actividades</dt>
		<dd/>
		<c:forEach var="actividad" items="${departamento.actividades}">
			<dt/><dd><a href="<c:url value="/actividades/${actividad.codigo}"/>">${actividad.codigo}</a></dd>
		</c:forEach>
		<dt>Grupos</dt>
		<dd/>
	</dl>
</div>