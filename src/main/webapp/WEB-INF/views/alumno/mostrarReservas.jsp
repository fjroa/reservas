<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link href="<c:url value="/css/calendario.css"/>" rel="stylesheet"
	type="text/css" media="screen, print" />
<!-- Javascript -->
<script type="text/javascript" src="<c:url value="/js/calendario.js"/> " /></script>

<h2>
	<s:message code='alumno.informacionReservas.titulo' />
</h2>

<div id="asignaturas">
	<h2>
		<s:message code='alumno.informacionReservas.reservas' />
	</h2>
	<c:choose>
		<c:when test="${empty listaReservas}">
			<h4>El alumno no tiene reservas activas.</h4>
		</c:when>
		<c:otherwise>
			<ul class="lista_asignaturas">
				<c:forEach var="reserva" items="${listaReservas}"
					varStatus="reservaCounter">
					<li class="asignatura">
						<h3>
							${reserva.reserva.grupo.actividad.nombre} <span class="expand"> </span>
						</h3>
						<h4 class="detalle_asignatura">
							<dl>
								<dt>Estado de la Reserva :</dt>
								<dd>${reserva.reserva.estadoReserva}</dd>
								<dt>Horario :</dt>
								<c:forEach var="periodo"
									items="${reserva.reserva.grupo.periodos}" varStatus="status">
									${not status.first ? '<dt></dt>' : ''}
									<dd>
										Del
										<fmt:formatDate value="${periodo.fechaInicio}"
											pattern="dd/MM/yyyy" />
										al
										<fmt:formatDate value="${periodo.fechaFin}"
											pattern="dd/MM/yyyy" />
									</dd>
									<c:forEach var="sesion" items="${periodo.sesiones}">
										<dt></dt>
										<dd>
											<s:message code="calendario.dia{${sesion.dia.valor}}" />
											de
											<fmt:formatNumber minIntegerDigits="2"
												value="${sesion.horaInicio}" />:<fmt:formatNumber minIntegerDigits="2"
												value="${sesion.minutoInicio}" />
											a
											<fmt:formatNumber minIntegerDigits="2"
												value="${sesion.horaInicio+sesion.horaDuracion}" />:<fmt:formatNumber minIntegerDigits="2"
												value="${sesion.minutoInicio+sesion.minutoDuracion}" />
										</dd>
									</c:forEach>
								</c:forEach>
							</dl>
						</h4>
						<ul class="MAS_INFO">
							<li class="MAS_INFO">

								<table class="bluetable3">

									<thead>
										<TR>
											<TH COLSPAN="5"><h4 style="text-align: center">Historico
													de la reserva</h4></TH>
										</TR>
										<tr>
											<th>Acci�n</th>
											<th>Fecha</th>
											<th>Usuario</th>
											<th>Estado</th>
											<th>Comentario</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="historico"
											items="${reserva.reserva.historicosReserva}">
											<tr>
												<td><s:message code="accionReserva.${historico.accionReserva}"/></td>
												<td><fmt:formatDate value="${historico.fechaAccion}"
														pattern="dd/MM/yyyy" /> <fmt:formatDate
														value="${historico.fechaAccion}" pattern="HH:mm:ss" /></td>
												<td>${historico.usuario.nombre}</td>
												<td>${historico.estadoReserva}</td>
												<td><c:choose>
														<c:when test="${empty historico.comentario}">
													-
												</c:when>
														<c:otherwise>
													${historico.comentario}
											</c:otherwise>
													</c:choose></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</li>
						</ul>
					</li>
				</c:forEach>
			</ul>
		</c:otherwise>
	</c:choose>
</div>
<div id="calendario">
	<h2>
		<s:message code='calendario.titulo' />
	</h2>
	<c:forEach var="dia" items="${calendario.items}" varStatus="diaCounter">
		<ul id="dia${diaCounter.index}">
			<li class="dia"><span><s:message
						code='calendario.dia{${diaCounter.index}}' /></span></li>
			<c:forEach var="item" items="${dia}">
				<li class="${item.clase}">${item.texto}</li>
			</c:forEach>
		</ul>
	</c:forEach>
</div>