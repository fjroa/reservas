<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link href="<c:url value="/css/calendario.css"/>" rel="stylesheet"
	type="text/css" media="screen, print" />

<h2>Alumno ${alumno.idUsuario}</h2>
<div id="detalle">
	<dl>
		<dt>ID</dt>
		<dd>${alumno.idUsuario}</dd>
		<dt>Nombre</dt>
		<dd>${alumno.nombre}</dd>
		<dt>Promedio</dt>
		<dd>${alumno.promedio}</dd>
		<dt>Actividades</dt>
		<dd/>
		<c:forEach var="actividad" items="${alumno.actividades}">
			<dt/><dd><a href="<c:url value="/actividades/${actividad.codigo}"/>">${actividad.codigo}</a></dd>
		</c:forEach>
		<dt>Grupos Teor�a</dt><dd><a href="<c:url value="/alumnos/${alumno.idUsuario}/gruposTeoria"/>">Ver</a></dd>
		<dt>Reservas</dt><dd><a href="<c:url value="/alumnos/${alumno.idUsuario}/reservas"/>">Ver</a></dd>
	</dl>
</div>